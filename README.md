# P23R Client

## Build and install the client

Prerequisites:

* JDK 1.8 or newer
* eXist-db 2.2
* WildFly 8.1.0.Final

In order to build the P23R client you have to install the P23R server into a maven repository, e.g. the local one:

```
$> cd p23r-server
$> mvn install
```

How to checkout and build the P23R server see README.md in the P23R server directory at 
https://gitlab.com/openp23r/p23r-server.

Then, to build and deploy the client from source:

```
$> git clone https://gitlab.com/openp23r/p23r-client.git
$> cd p23r-client
$> mvn package wildfly:deploy
```

It is necessary that all servers (eXist-db, WildFly) are running. Otherwise the deployment will fail.

## Legal issues

This project is part of the openP23R initiative. All related projects are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see LICENSE file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS

By submitting a "pull request" or otherwise contributing to this repository, you agree to license your contribution under the license mentioned above.

