package de.p23r.client.portal.protocolpool;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;

/**
 * The Class ProtocolFilter.
 *
 * @author sim
 */
@Named
@SessionScoped
public class ProtocolFilter implements Serializable {

    private static final String PROTOCOL_FACES_REDIRECT_TRUE = "protocol?faces-redirect=true";

    /**
     * 
     */
    private static final long serialVersionUID = -2217793172708026689L;

    private FilterCriteria filterCriteria;

    /**
     * Inits the filter criteria.
     */
    @PostConstruct
    public void init() {
        filterCriteria = new FilterCriteria();
        filterCriteria.setOffset(0D);
        filterCriteria.setMax(10);
        filterCriteria.setSorting("recordAt descending");
    }

    /**
     * Resets the filter criteria.
     */
    public void reset() {
        init();
    }

    /**
     * Gets the filter criteria.
     *
     * @return the filter criteria
     */
    public FilterCriteria getFilterCriteria() {
        return filterCriteria;
    }

    /**
     * Filter notification.
     *
     * @param notificationId the notification id
     * @return the string
     */
    public String filterNotification(String notificationId) {
        reset();
        filterCriteria.setNotificationId(notificationId);
        return PROTOCOL_FACES_REDIRECT_TRUE;
    }

    /**
     * Filter message.
     *
     * @param messageId the message id
     * @return the string
     */
    public String filterMessage(String messageId) {
        reset();
        filterCriteria.setMessageId(messageId);
        return PROTOCOL_FACES_REDIRECT_TRUE;
    }

    /**
     * Filter transaction.
     *
     * @param transactionId the transaction id
     * @return the string
     */
    public String filterTransaction(String transactionId) {
        reset();
        filterCriteria.setTransactionId(transactionId);
        return PROTOCOL_FACES_REDIRECT_TRUE;
    }

}
