package de.p23r.client.portal.protocolpool;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;

/**
 * The Class ProtocolQueryManager.
 */
@Named
@ViewScoped
public class ProtocolQueryManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7934684163945435686L;

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private ProtocolQueryService protocolQueryService;

    @Inject
    private ProtocolFilter protocolFilter;

    private LazyDataModel<ProtocolEntryBean> model;
    
    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
    	model = new LazyDataModel<ProtocolEntryBean>() {    		
    		/**
			 * 
			 */
			private static final long serialVersionUID = 6356959240405078498L;
			@Override
			public Object getRowKey(ProtocolEntryBean object) {
				return object.getEntry().getRecordId();
			}

			@Override
			public ProtocolEntryBean getRowData(String rowKey) {
				Iterator<ProtocolEntryBean> it = iterator();
				while (it.hasNext()) {
					ProtocolEntryBean entry = it.next();
					if (it.next().getEntry().getRecordId().equals(rowKey)) {
						return entry;
					};
				}
				return super.getRowData(rowKey);
			}
			
			@Override
    		public List<ProtocolEntryBean> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
                try {
                	FilterCriteria criteria = protocolFilter.getFilterCriteria();
                	criteria.setMax(pageSize);
                	criteria.setOffset((double) first);
					return protocolQueryService.queryProtocolPool(criteria);
				} catch (P23RAppFault_Exception e) {
					log.error("query protocol pool", e);
				}
                return Collections.emptyList();
    		}
    	};
    	
    	FilterCriteria criteria = new FilterCriteria();
    	criteria.setMax(1);
    	criteria.setOffset(0D);
		try {
			protocolQueryService.queryProtocolPool(criteria);
	    	model.setRowCount(protocolQueryService.getLastResult().getCount().intValue());
		} catch (P23RAppFault_Exception e) {
			log.error("query protocol pool", e);
		}

    }
    
    /**
     * Returns all available protocol entries.
     *
     * @return entries
     */
    public LazyDataModel<ProtocolEntryBean> getProtocolEntries() {
    	return model;
    }

}
