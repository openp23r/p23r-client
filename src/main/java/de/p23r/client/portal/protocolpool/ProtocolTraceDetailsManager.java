package de.p23r.client.portal.protocolpool;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.Trace;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;

/**
 * The Class ProtocolTraceDetailsManager.
 */
@Named
@RequestScoped
public class ProtocolTraceDetailsManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7934684163945435686L;

    private final Logger log = LoggerFactory.getLogger(getClass());
    
    @Inject
    private ProtocolQueryService service;

    /**
     * The index of the entry that is displayed as string
     */
    private String id;

    /**
     * The index of the entry as integer
     */
    private int index;

    /**
     * The entry
     */
    private ProtocolEntryBean bean;

    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
        if (id == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            id = params.get("id");
        } else {
        	bean = null;
        	return;
        }
        try {
            FilterCriteria criteria = new FilterCriteria();
            criteria.setRecordId(id);
            bean = service.queryProtocolPool(criteria).get(0);
        } catch (NumberFormatException e) {
            log.error("id is not a number", e);
            bean = null;
        } catch (P23RAppFault_Exception e) {
            log.error("query protocol entry", e);
			bean = null;
		}
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Gets the bean.
     *
     * @return the bean
     */
    public ProtocolEntryBean getBean() {
        return bean;
    }

    /**
     * Checks for trace.
     *
     * @return true, if successful
     */
    public boolean hasTrace() {
        return bean != null && bean.hasTrace();
    }

    /**
     * Gets the trace.
     *
     * @return the trace
     */
    public Trace getTrace() {
        if (bean != null) {
            return bean.getEntry().getContent().getTrace();
        }
        return null;
    }

}
