package de.p23r.client.portal.protocolpool;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.IProtocolQuery;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.IProtocolQueryService;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.FilterCriteria;
import de.p23r.leitstelle.ns.p23r.iprotocolquery1_1.types.QueryResult;

/**
 * The Class ProtocolQueryService.
 * 
 * @author ara
 */
@Named
@ApplicationScoped
public class ProtocolQueryService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5102120979656393652L;

    private static final transient Logger LOG = LoggerFactory.getLogger(ProtocolQueryService.class);

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    private transient IProtocolQuery port;

    private QueryResult lastResult;
    
    @PostConstruct
    private synchronized void initService() {
        String url = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IPROTOCOLQUERY_URL, IProtocolQueryService.WSDL_LOCATION.toString());
        String serviceName = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IPROTOCOLQUERY_SERVICE, IProtocolQueryService.SERVICE.getLocalPart());
        URL wsdlUrl = null;
        try {
            wsdlUrl = new URL(url);
        } catch (MalformedURLException e) {
            LOG.error("Failed to create url in service initialization.", e);
        }
        QName serviceQName = new QName(IProtocolQueryService.SERVICE.getNamespaceURI(), serviceName);
        Service service = IProtocolQueryService.create(wsdlUrl, serviceQName);

        port = service.getPort(IProtocolQuery.class);
        LOG.debug("ProtocolQueryService -> initService");
    }

    /**
     * Query the ProtocolPool.
     *
     * @param filterCriteria the filter criteria
     * @return the list
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public synchronized List<ProtocolEntryBean> queryProtocolPool(FilterCriteria filterCriteria)
            throws P23RAppFault_Exception {

        try {
            lastResult = port.queryProtocol(filterCriteria);
            List<ProtocolEntry> list = lastResult.getProtocolEntries();
//            list = filterList(list);
            return formatMessages(list);
        } catch (P23RAppFault_Exception e) {
            LOG.error("Exception while queueProtocolEntries ", e);
        }

        /* Return an empty list if we reached this point (previous exception occurred) */
        return Collections.emptyList();
    }

    /**
     * Format messages.
     *
     * @param list the list
     * @return the list
     */
    public List<ProtocolEntryBean> formatMessages(List<ProtocolEntry> list) {
        List<ProtocolEntryBean> result = new ArrayList<ProtocolEntryBean>();

        for (ProtocolEntry item : list) {
            ProtocolEntryBean bean = new ProtocolEntryBean();
            bean.setEntry(item);
            result.add(bean);
        }

        return result;
    }

    /**
     * Gets the last result.
     *
     * @return the last result
     */
    public QueryResult getLastResult() {
    	return lastResult;
    }
    
}
