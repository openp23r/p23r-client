package de.p23r.client.portal.protocolpool;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import de.p23r.leitstelle.ns.p23r.commonprotocol1_0.ProtocolEntry;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;

/**
 * The Class ProtocolEntryBean.
 */
public class ProtocolEntryBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 5539331379450548191L;

    private ProtocolEntry entry;

    /**
     * Gets the entry.
     *
     * @return the entry
     */
    public ProtocolEntry getEntry() {
        return entry;
    }

    /**
     * Sets the entry.
     *
     * @param entry the new entry
     */
    public void setEntry(ProtocolEntry entry) {
        this.entry = entry;
    }

    /**
     * Checks for details.
     *
     * @return true, if successful
     */
    public boolean hasDetails() {
        if (entry.getMessageId() != null || entry.getNotificationId() != null || entry.getRuleId() != null) {
            return true;
        }
        if (entry.getRuleName() != null || entry.getTransactionId() != null || hasTrace()) {
            return true;
        }
        return false;
    }

    /**
     * Gets the image name.
     *
     * @return the image name
     */
    public String getImageName() {
        String image = null;
        if (entry.getContent().getLog() != null) {
            switch (entry.getContent().getLog().getLevel()) {
            case ERROR:
                image = "exclamation-icon.png";
                break;
            case WARNING:
                image = "warning.png";
                break;
            case TEST_SUCCESS:
                image = "success.png";
                break;
            case TEST_FAIL:
                image = "failure.png";
                break;
            case SECURITY:
                image = "security.png";
                break;
            case FATAL:
                image = "fatal.png";
                break;
            case INFO:
            default:
                image = "info.png";
                break;
            }
        } else if (entry.getContent().getTrace() != null) {
            image = "trace.png";
        } else {
            image = "questionmark.png";
        }
        return image;
    }

    /**
     * Checks for log.
     *
     * @return true, if successful
     */
    public boolean hasLog() {
        return entry.getContent().getLog() != null;
    }

    /**
     * Checks for trace.
     *
     * @return true, if successful
     */
    public boolean hasTrace() {
        return entry.getContent().getTrace() != null;
    }

    /**
     * Checks for generic content.
     *
     * @return true, if successful
     */
    public boolean hasGenericContent() {
        return entry.getContent().getGenericContent() != null;
    }
    
    /**
     * Gets the log content.
     *
     * @return the log content
     */
    public String getLogContent() {
        List<Description> descriptions = entry.getContent().getLog().getNotes();
        return getDescription(descriptions, Locale.getDefault());        
    }
    
    /**
     * Gets the trace content.
     *
     * @return the trace content
     */
    public String getTraceContent() {
        List<Description> descriptions = entry.getContent().getTrace().getNotes();
        return getDescription(descriptions, Locale.getDefault());        
    }
    
    /**
     * Return the description content for the given language if available or any or null if not.
     * @param descriptions
     * @param locale
     * @return content
     */
    private String getDescription(List<Description> descriptions, Locale locale) {
        for (Description desc : descriptions) {
            if (locale.getLanguage().equals(new Locale(desc.getLang()).getLanguage())) {
                return desc.getContent();
            }
        }
        //TODO return or return not any available content if there is no one for the current language?
        if (!descriptions.isEmpty()) {
            return descriptions.get(0).getContent();
        }
        return null;
    }
}
