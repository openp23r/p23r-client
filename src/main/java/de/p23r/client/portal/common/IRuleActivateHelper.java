package de.p23r.client.portal.common;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IRuleActivate;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.IRuleActivateService;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateResultType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;

/**
 * The Class IRuleActivateHelper.
 */
@Named
@ApplicationScoped
public class IRuleActivateHelper implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -9071190437459912158L;

	private final transient Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private P23RXmlDbClientConfig p23rXmlDbClientConfig;

	private transient IRuleActivate ruleActivate;

	/**
	 * Update notification rule packages.
	 *
	 * @param note            the note
	 * @param tenant            the tenant
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	public void updateNotificationRulePackages(String note, String tenant) throws P23RAppFault_Exception {
		ruleActivate.updateNotificationRulePackages(note, tenant);
	}

	/**
	 * Gets the notification rule package states.
	 *
	 * @param tenant            the tenant
	 * @return the notification rule package states
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	public RulePackageStateResultType getNotificationRulePackageStates(String tenant) throws P23RAppFault_Exception {
		return ruleActivate.getNotificationRulePackageStates(tenant);
	}

	/**
	 * Sets the notification rule package states.
	 *
	 * @param rulePackageList            the rule package list
	 * @param tenant            the tenant
	 * @param annotation the annotation
	 * @return the rule package state result type
	 * @throws P23RAppFault_Exception the p23 r app fault_ exception
	 */
	public List<RulePackageStateType> setNotificationRulePackageStates(List<RulePackageStateType> rulePackageList, String tenant, String annotation)
			throws P23RAppFault_Exception {
		RulePackageStateResultType result = ruleActivate.setNotificationRulePackageStates(rulePackageList, tenant, annotation);
		return result.getPackageStates();
	}

	/**
	 * Inits the {@link IRuleActivateService} port.
	 */
	@PostConstruct
	public void init() {
		String url = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IRULEACTIVATE_URL, IRuleActivateService.WSDL_LOCATION.toString());
		String serviceName = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IRULEACTIVATE_SERVICE, IRuleActivateService.SERVICE.getLocalPart());
		URL wsdlUrl = null;
		try {
			wsdlUrl = new URL(url);
		} catch (MalformedURLException e) {
			log.error(url, e);
		}

		QName serviceQName = new QName("http://leitstelle.p23r.de/NS/P23R/IRuleActivate1-0", serviceName);
		IRuleActivateService service = new IRuleActivateService(wsdlUrl, serviceQName);

		ruleActivate = service.getPort(IRuleActivate.class);
	}

}
