package de.p23r.client.portal.common;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;

/**
 * The Class P23RFault.
 */
public abstract class P23RFault {

    /**
     * Hide public default constructor
     */
    private P23RFault() {
    }

    /**
     * Creates a P23RAppFault for a given description and error code.
     *
     * @param description the description
     * @param exception the exception
     * @param errorCode the error code
     * @return the P23RAppFault
     */
    public static P23RAppFault_Exception createP23RAppFault(String description, Exception exception, String errorCode) {
        P23RAppFault faultInfo = new P23RAppFault();

        if (exception != null) {
            description = description + " -> " + exception.getMessage();
        }
        faultInfo.setDescription(description);
        return new P23RAppFault_Exception(description, faultInfo);
    }
}
