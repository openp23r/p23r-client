package de.p23r.client.portal.common;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.IMessageDeliverLocal;
import de.p23r.leitstelle.ns.p23r.imessagedeliverlocal1_0.IMessageDeliverLocalService;

/**
 * The Class IMessageDeliverLocalHelper.
 */
@Named
@ApplicationScoped
public class IMessageDeliverLocalHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2663795621308135133L;

    private final transient Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    private transient IMessageDeliverLocal deliverLocal;

    /**
     * Inits the {@link IMessageDeliverLocalService} port.
     */
    @PostConstruct
    public void init() {
        String url = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IMESSAGEDELIVERLOCAL_URL, IMessageDeliverLocalService.WSDL_LOCATION.toString());
        String serviceName = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_IMESSAGEDELIVERLOCAL_SERVICE, IMessageDeliverLocalService.SERVICE.getLocalPart());
        try {
            URL wsdlUrl = new URL(url);
            QName serviceQName = new QName(IMessageDeliverLocalService.SERVICE.getNamespaceURI(), serviceName);
            IMessageDeliverLocalService service = new IMessageDeliverLocalService(wsdlUrl, serviceQName);

            deliverLocal = service.getPort(IMessageDeliverLocal.class);
        } catch (MalformedURLException e) {
            log.error(url, e);
        }
    }

    /**
     * Delivers the message.
     *
     * @param message the message
     * @param tenant the tenant
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void deliverMessage(Object message, String tenant) throws P23RAppFault_Exception {
        if (deliverLocal != null) {
            deliverLocal.deliverMessage(message, tenant);
        } else {
            throw new P23RAppFault_Exception("message deliver proxy error");
        }
    }

}
