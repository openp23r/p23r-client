package de.p23r.client.portal.common;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.leitstelle.ns.p23r.common1_0.Notification;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.INotificationApprove;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.INotificationApproveService;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.types.Approval;

/**
 * The Class INotificationApproveHelper.
 */
@Named
@ApplicationScoped
public class INotificationApproveHelper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7375964838181943244L;

    private final transient Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    private transient INotificationApprove notificationApprove;

    /**
     * Gets the notification.
     *
     * @param notificationId the notification id
     * @return the notification
     */
    public Notification getNotification(String notificationId) {
        try {
            return notificationApprove.getNotification(notificationId);
        } catch (P23RAppFault_Exception | SOAPFaultException e) {
            log.error("Failed to get notification.", e);
        }
        return null;
    }

    /**
     * Gets the message.
     *
     * @param notificationId the notification id
     * @return the message
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public Object getMessage(String notificationId) throws P23RAppFault_Exception {
        return notificationApprove.getMessage(notificationId);
    }

    /**
     * Approve notification.
     *
     * @param approval the approval
     * @param notification the notification
     * @param notificationId the notification id
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void approveNotification(Approval approval, String notification, String notificationId) throws P23RAppFault_Exception {
        log.debug("modified notification to approve: {}", notification);
        Notification not = new Notification();
//        not.setContent(new StreamSource(new ByteArrayInputStream(notification.getBytes(Charset.forName("UTF-8")))));
        not.setContent(new DataHandler(new ByteArrayDataSource(notification.getBytes(Charset.forName("UTF-8")), "application/xml")));

        notificationApprove.approveNotification(approval, not, notificationId);
    }

    /**
     * Inits the {@link INotificationApproveService} port.
     */
    @PostConstruct
    public void init() {
        String wsdlUrl = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_INOTIFICATIONAPPROVE_URL, INotificationApproveService.WSDL_LOCATION.toString());
        String serviceName = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_INOTIFICATIONAPPROVE_SERVICE,
                INotificationApproveService.SERVICE.getLocalPart());
        URL url = null;
        try {
            url = new URL(wsdlUrl);
        } catch (MalformedURLException e) {
            log.error(wsdlUrl, e);
        }

        QName serviceQName = new QName(INotificationApproveService.SERVICE.getNamespaceURI(), serviceName);
        INotificationApproveService service = new INotificationApproveService(url, serviceQName);

        notificationApprove = service.getPort(INotificationApprove.class);
        BindingProvider provider = (BindingProvider) notificationApprove;
        ((SOAPBinding) provider.getBinding()).setMTOMEnabled(true);
    }
}
