package de.p23r.client.portal.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ResourceLoader.
 */
public abstract class ResourceLoader {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceLoader.class);

    /**
     * Hide public default constructor
     */
    private ResourceLoader() {
    }

    /**
     * Loads a resource file on a webserver context path. Example: byte[] data =
     * loadResource("/WEB-INF/classes/myfile.bin");
     *
     * @param relativePath the relative path
     * @return byte[] of the data content
     */
    public static byte[] loadResource(String relativePath) {
        byte[] buffer;
        FileInputStream f = null;
        try {

            FacesContext facesContext = FacesContext.getCurrentInstance();

            ServletContext ctx = (ServletContext) facesContext.getExternalContext().getContext();
            String ctxpath = new File(ctx.getRealPath("/")).getAbsolutePath();
            File resourceFile = new File(ctxpath + relativePath);
            buffer = new byte[(int) resourceFile.length()];

            f = new FileInputStream(resourceFile.getAbsolutePath());
            f.read(buffer);
            return buffer;
        } catch (FileNotFoundException e) {
            LOG.error("Failed to load resource because file could not be found.", e);
            return new byte[0];
        } catch (IOException e) {
            LOG.error("Failed to load resource.", e);
            return new byte[0];
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException e) {
                    LOG.error("Failed to close file.", e);
                }
            }
        }
    }
}
