package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.datatype.XMLGregorianCalendar;

import org.picketlink.Identity;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.client.ns.ruleinfos.RuleInfo;
import de.p23r.client.portal.notification.XmlDbManager;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;

/**
 * The Class RuleManager.
 */
@Named
@SessionScoped
public class RuleManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6226169137874442050L;

    private static final transient Logger LOG = LoggerFactory.getLogger(RuleManager.class);

    @Inject
    private RulePackageListManager rulePackageListManager;

    @Inject
    private XmlDbManager xmldbManager;

    @Inject
    private Identity identity;

    private RuleBean rule;

    private TimelineModel timelineModel;

    private boolean autoApproval;

    private static String[] days = { "Montags", "Dienstags", "Mittwochs", "Donnerstags", "Freitags",
            "Samstags", "Sonntags" };

    /**
     * View rule.
     *
     * @param rule the rule
     * @return the string
     */
    public String viewRule(RuleBean rule) {
        this.rule = rule;
        RuleInfo ruleInfo = xmldbManager.getRuleInfo(rule.getId());
        autoApproval = ruleInfo != null ? ruleInfo.isAutoNotification() : false;

        GregorianCalendar from = rule.getValidFrom().toGregorianCalendar();
        GregorianCalendar to = rule.getValidUntil().toGregorianCalendar();

        timelineModel = new TimelineModel();

        List<XMLGregorianCalendar> calendars = rule.getSubmitAts();
        // add events
        for (XMLGregorianCalendar calendar : calendars) {
            GregorianCalendar current = calendar.toGregorianCalendar();
            if (from.before(current) && to.after(current)) {
                timelineModel.add(createEvent(current, calendar));
            }
        }
        // years
        for (int y = from.get(Calendar.YEAR); y <= to.get(Calendar.YEAR); y++) {
            calendars = rule.getSubmitAtYearlyPeriods();
            timelineModel.addAll(getEvents(calendars, y, null, null, from, to));
            // months
            for (int m = from.get(Calendar.MONTH); m <= to.get(Calendar.MONTH); m++) {
                calendars = rule.getSubmitAtMonthlyPeriods();
                timelineModel.addAll(getEvents(calendars, y, m, null, from, to));
                // days
                for (int d = from.get(Calendar.DAY_OF_MONTH); d <= to.get(Calendar.DAY_OF_MONTH); d++) {
                    calendars = rule.getSubmitAtDailyPeriods();
                    timelineModel.addAll(getEvents(calendars, y, m, d, from, to));
                }
            }
        }
        return "/secured/packagerule?faces-redirect=true";
    }

    private TimelineEvent createEvent(GregorianCalendar current, XMLGregorianCalendar calendar) {
        return new TimelineEvent("<div class='processIcon' title='"
                + new SimpleDateFormat("d. MMM yyyy mm:HH").format(current.getTime()) + "' />", calendar
            .toGregorianCalendar().getTime());
    }

    private List<TimelineEvent> getEvents(List<XMLGregorianCalendar> calendars, Integer y, Integer m,
            Integer d, GregorianCalendar from, GregorianCalendar to) {
        List<TimelineEvent> events = new ArrayList<TimelineEvent>();
        for (XMLGregorianCalendar calendar : calendars) {
            GregorianCalendar current = calendar.toGregorianCalendar();
            if (y != null) {
                current.set(Calendar.YEAR, y);
            }
            if (m != null) {
                current.set(Calendar.MONTH, m);
            }
            if (d != null) {
                current.set(Calendar.DAY_OF_MONTH, d);
            }
            if (from.before(current) && to.after(current)) {
                events.add(createEvent(current, calendar));
            }
        }
        return events;
    }

    /**
     * Gets the rule.
     *
     * @return the rule
     */
    public RuleBean getRule() {
        return rule;
    }

    /**
     * Deactivates a rulegroup in MARM.
     *
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void deactivateRule() throws P23RAppFault_Exception {
        LOG.debug("deactivate rule with ID {}", rule.getId());
        List<RulePackageStateType> states = rulePackageListManager.getRulePackageStates();

        for (RulePackageStateType packageState : states) {
            for (RuleGroupStateType groupState : packageState.getRuleGroups()) {
                for (RuleStateType ruleState : groupState.getRules()) {
                    if (ruleState.getId().equals(rule.getId())) {
                        ruleState.setIsDeactivated(true);
                        break;
                    }
                }
            }
        }

        states = rulePackageListManager.setNotificationRulePackageStates(states);

        for (RulePackageStateType packageState : states) {
            for (RuleGroupStateType groupState : packageState.getRuleGroups()) {
                for (RuleStateType ruleState : groupState.getRules()) {
                    if (ruleState.getId().equals(rule.getId())) {
                        rule.setState(ruleState);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Checks if is auto approval.
     *
     * @return true, if is auto approval
     */
    public boolean isAutoApproval() {
        return autoApproval;
    }

    /**
     * Sets the auto approval.
     *
     * @param autoApproval the new auto approval
     */
    public void setAutoApproval(boolean autoApproval) {
        this.autoApproval = autoApproval;
    }

    /**
     * Toggle auto approval.
     *
     * @param e the e
     */
    public void toggleAutoApproval(ValueChangeEvent e) {
        autoApproval = Boolean.getBoolean(e.getNewValue().toString());
        LOG.debug("toggle auto approval to: {}", e.getNewValue());
        RuleInfo ruleInfo = xmldbManager.getRuleInfo(rule.getId());
        if (autoApproval) {
            ruleInfo.setAutoNotificationUser(identity.getAccount().getId());
            // TODO this is wrong. if we know how to authenticate we have to change this...
            ruleInfo.setAutoNotificationPassword("unknown");
        }
        ruleInfo.setAutoNotification(autoApproval);
        xmldbManager.updateRuleInfo(ruleInfo);
        String msg = autoApproval ? "Automatische Freigabe erfolgreich aktiviert."
                : "Automatische Freigabe erfolgreich deaktiviert.";
        FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
    }

    /**
     * Helper, creates a comma seperated string from a calendar list.
     *
     * @param list the list
     * @param format the format
     * @return the comman sperated string
     */
    public List<String> formatCalendars(List<XMLGregorianCalendar> list, String format) {
        if (list != null && !list.isEmpty()) {
            List<String> result = new ArrayList<String>(list.size());
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            for (XMLGregorianCalendar x : list) {
                result.add(sdf.format(x.toGregorianCalendar().getTime()));
            }
            return result;
        }

        return Collections.emptyList();
    }

    /**
     * Gets the submit at daily periods.
     *
     * @return the submit at daily periods
     */
    public List<String> getSubmitAtDailyPeriods() {
        List<XMLGregorianCalendar> calendars = rule.getSubmitAtDailyPeriods();
        return formatCalendars(calendars, "HH:mm");
    }

    /**
     * Gets the submit at weekly periods.
     *
     * @return the submit at weekly periods
     */
    public String getSubmitAtWeeklyPeriods() {
        List<Integer> calendars = rule.getSubmitAtWeeklyPeriods();
        StringBuilder weekly = new StringBuilder();
        for (Integer i : calendars) {
            if (weekly.length() > 0) {
                weekly.append(", ");
            }
            weekly.append(days[i - 1]);
        }
        return weekly.toString();
    }

    /**
     * Gets the submit at monthly periods.
     *
     * @return the submit at monthly periods
     */
    public List<String> getSubmitAtMonthlyPeriods() {
        List<XMLGregorianCalendar> calendars = rule.getSubmitAtMonthlyPeriods();
        return formatCalendars(calendars, "d");
    }

    /**
     * Gets the submit at yearly periods.
     *
     * @return the submit at yearly periods
     */
    public List<String> getSubmitAtYearlyPeriods() {
        List<XMLGregorianCalendar> calendars = rule.getSubmitAtYearlyPeriods();
        return formatCalendars(calendars, "d. MMM");
    }

    /**
     * Gets the submit ats.
     *
     * @return the submit ats
     */
    public List<String> getSubmitAts() {
        List<XMLGregorianCalendar> calendars = rule.getSubmitAts();
        return formatCalendars(calendars, "d. MMM yyyy HH:mm");
    }

    /**
     * Gets the model.
     *
     * @return the model
     */
    public TimelineModel getModel() {
        return timelineModel;
    }

    /**
     * Gets the rule name.
     *
     * @param ruleId the rule id
     * @return the rule name
     */
    public String getRuleName(String ruleId) {
        RuleInfo ruleInfo = xmldbManager.getRuleInfo(ruleId);
        RuleManifest manifest = SchemaHelperFactory.getNotificationRuleManifestSchemaHelper().create(
            ruleInfo.getManifest());
        return manifest.getTitle();
    }

}
