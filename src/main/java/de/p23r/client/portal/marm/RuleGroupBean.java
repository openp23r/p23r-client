package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;
import de.p23r.leitstelle.ns.p23r.nrl.rulegroupmanifest1_1.RuleGroupManifest;

/**
 * The Class RuleGroupBean.
 */
public class RuleGroupBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 8815778560801122820L;

    private RuleGroupStateType state;
    private NotificationRulePackageArchive archive;
    private RuleGroupManifest manifest;
    private String path;

    /**
     * Instantiates a new rule group bean.
     *
     * @param state the state
     * @param archive the archive
     */
    public RuleGroupBean(RuleGroupStateType state, NotificationRulePackageArchive archive) {
        this.state = state;
        this.archive = archive;

        List<String> groupPaths = this.archive.getNotificationRuleGroupPathes();
        for (String groupPath : groupPaths) {
            if (state.getId().equals(this.archive.getNotificationRuleGroupManifestHelper(groupPath).getId())) {
                manifest = this.archive.getNotificationRuleGroupManifestHelper(groupPath);
                path = groupPath;
            }
        }
    }

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    public void setState(RuleGroupStateType state) {
        this.state = state;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return manifest.getId();
    }

    /**
     * Gets the rules.
     *
     * @return the rules
     */
    public List<RuleBean> getRules() {
        List<RuleBean> rules = new ArrayList<RuleBean>();
        for (RuleStateType ruleState : state.getRules()) {
            rules.add(new RuleBean(ruleState, archive, path));
        }
        return rules;
    }

    /**
     * Checks if is active.
     *
     * @return true, if is active
     */
    public boolean isActive() {
        return state.isIsActive();
    }

    /**
     * Checks if is activated.
     *
     * @return true, if is activated
     */
    public boolean isActivated() {
        return state.isIsActivated();
    }

    /**
     * Checks if is recommended.
     *
     * @return true, if is recommended
     */
    public boolean isRecommended() {
        return state.isIsRecommended();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return manifest.getName();
    }

    /**
     * Gets the release.
     *
     * @return the release
     */
    public String getRelease() {
        return manifest.getRelease();
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return manifest.getTitle();
    }

    /**
     * Gets the descriptions.
     *
     * @return the descriptions
     */
    public List<Description> getDescriptions() {
        return manifest.getDescriptions();
    }

    /**
     * Gets the hints.
     *
     * @return the hints
     */
    public List<Description> getHints() {
        return manifest.getHints();
    }

    /**
     * Gets the targets.
     *
     * @return the targets
     */
    public List<Description> getTargets() {
        return manifest.getTargets();
    }

    /**
     * Gets the licenses.
     *
     * @return the licenses
     */
    public List<Description> getLicenses() {
        return manifest.getLicenses();
    }

    /**
     * Gets the legal backgrounds.
     *
     * @return the legal backgrounds
     */
    public List<Description> getLegalBackgrounds() {
        return manifest.getLegalBackgrounds();
    }

    /**
     * Gets the creators.
     *
     * @return the creators
     */
    public List<String> getCreators() {
        return manifest.getCreators();
    }

    /**
     * Gets the subjects.
     *
     * @return the subjects
     */
    public List<String> getSubjects() {
        return manifest.getSubjects();
    }

    /**
     * Gets the warnings.
     *
     * @return the warnings
     */
    public List<String> getWarnings() {
        return state.getWarnings();
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return state.getErrors();
    }

}
