package de.p23r.client.portal.marm;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import de.p23r.client.portal.common.IMessageDeliverLocalHelper;
import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.common.modelandrulemanagement.SchemaHelper;
import de.p23r.common.modelandrulemanagement.SchemaHelperFactory;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;

/**
 * Handles the notification approval.
 */
@Named
@SessionScoped
public class RuleGenerateAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8893383498726430047L;

    /**
     * The Constant MAXDEPTH.
     */
    public static final int MAXDEPTH = 99999;

    private final transient Logger log = LoggerFactory.getLogger(getClass());

    private String message;

    @Inject
    private IMessageDeliverLocalHelper messageDeliverLocal;

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    private int actionRemoveNode = 0;

    /**
     * Gets the action remove node.
     *
     * @return the action remove node
     */
    public int getActionRemoveNode() {
        return actionRemoveNode;
    }

    /**
     * Sets the action remove node.
     *
     * @param actionRemoveNode the new action remove node
     */
    public void setActionRemoveNode(int actionRemoveNode) {
        this.actionRemoveNode = actionRemoveNode;
    }

    /**
     * Sets the message.
     *
     * @param message the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Xml to string.
     *
     * @param node the node
     * @return the string
     */
    public static String xmlToString(Node node) {
        return TransformerHelper.nodeToString(node, true);
    }

    /**
     * Triggered by UI. Loads the selected MessageXML and resets the states
     *
     * @param rule the rule
     */
    public void editMessage(RuleBean rule) {
        String messageSchema = rule.getMessageSchema();
        if (messageSchema != null) {
            SchemaHelper<Object> helper = SchemaHelperFactory.getDynamicSchemaHelper(messageSchema);
            message = helper.createDefaultInstance("message");
            log.debug("generated message: {}", message);
            actionRemoveNode = 0;
        }
    }

    /**
     * Sends the message (notification).
     *
     * @return the string
     */
    public String send() {
        String xml = message;

        log.debug("ModelAndRuleManagement - manual generate notification");
        log.debug(" - Message : " + xml);

        Element msg = TransformerHelper.elementFromString(xml);

        String tenant = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_TENANT, "default");
        try {
            messageDeliverLocal.deliverMessage(msg, tenant);
            return "/secured/notificationlist?faces-redirect=true";
        } catch (P23RAppFault_Exception e) {
            log.error("Message delivery failed.", e);
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_WARN, e.getFaultInfo().getCode(), e.getFaultInfo().getDescription()));
        } catch (SOAPFaultException e) {
            log.error("Message delivery failed.", e);
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getMessage()));
        }
        return null;
    }

}
