package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageListArchive;
import de.p23r.client.ns.ruleinfos.RuleInfo;
import de.p23r.client.portal.common.IRuleActivateHelper;
import de.p23r.client.portal.notification.XmlDbManager;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateResultType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.PackageList;
import de.p23r.leitstelle.ns.p23r.nrl.packagelist1_1.RulePackage;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;

/**
 * The Class RulePackageListManager.
 */
@Named
@SessionScoped
public class RulePackageListManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1970732549102428905L;

    private static final transient Logger LOG = LoggerFactory.getLogger(RulePackageListManager.class);

    @Inject
    private IRuleActivateHelper ruleActivate;

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    @Inject
    private XmlDbManager xmldbManager;

    private ModelAndRuleDepot depot;
    
    private List<RulePackageBean> packages = new ArrayList<RulePackageBean>();

    private List<RulePackageStateType> rulePackageList = new ArrayList<RulePackageStateType>();

    private List<String> controlCentres = new ArrayList<String>();

    private String updateRulesNote = "";

    private String tenant;

    /**
     * Inits the RulePackageListmanager.
     */
    @PostConstruct
    public void init() {
        tenant = p23rXmlDbClientConfig.get(P23RXmlDbClientConfig.XQUERY_TENANT, "default");
        
        String username = p23rXmlDbClientConfig.get(String.format(P23RXmlDbClientConfig.XQUERY_USERNAME, "default"), "p23r");
        String password = p23rXmlDbClientConfig.get(String.format(P23RXmlDbClientConfig.XQUERY_PASSWORD, "default"), "temp");

        try {
//        	ruleActivate.updateNotificationRulePackages("update for session", tenant);
            RulePackageStateResultType result = ruleActivate.getNotificationRulePackageStates(tenant);
    		controlCentres.clear();
    		controlCentres.addAll(result.getControlCentres());
    		rulePackageList.clear();
    		rulePackageList.addAll(result.getPackageStates());
        } catch (P23RAppFault_Exception e) {
            LOG.error("Failed to get the notification rule package states.", e);
        }

        if (!controlCentres.isEmpty() && controlCentres.get(0) != null) {
            String address = controlCentres.get(0);
            depot = ModelAndRuleDepot.create(address, username, password);
        }

//        updatePackageList();
    }

    private void propagateRuleInfo(NotificationRulePackageArchive archive) {
        for (String ruleGroupPath : archive.getNotificationRuleGroupPathes()) {
            for (String rulePath : archive.getNotificationRulePathes(ruleGroupPath)) {
                RuleManifest ruleManifest = archive.getNotificationRuleManifestHelper(rulePath);
                RuleInfo ruleInfo = xmldbManager.getRuleInfo(ruleManifest.getId());
                if (ruleInfo == null) {
                    ruleInfo = new RuleInfo();
                    ruleInfo.setRuleID(ruleManifest.getId());
                    ruleInfo.setAutoNotification(false);
                    ruleInfo.setRulePackageID(archive.getNotificationRulePackageManifestHelper().getId());
                }
                ruleInfo.setManifest(archive.getNotificationRuleManifest(rulePath));
                xmldbManager.updateRuleInfo(ruleInfo);
            }
        }
    }

    /**
     * Gets the packages.
     *
     * @return the packages
     */
    public List<RulePackageBean> getPackages() {
        return packages;
    }

    /**
     * Gets the rule package states.
     *
     * @return the rule package states
     */
    public List<RulePackageStateType> getRulePackageStates() {
        return rulePackageList;
    }

    /**
     * Gets the control centres.
     *
     * @return the control centres
     */
    public List<String> getControlCentres() {
        return controlCentres;
    }

    /**
     * Getter for the notes field for IRuleActivate.updateNotificationRulePackages
     *
     * @return the current content of the notes field
     */
    public String getUpdateRulesNote() {
        return updateRulesNote;
    }

    /**
     * Setter for the notes field for IRuleActivate.updateNotificationRulePackages
     *
     * @param value the new update rules note
     */
    public void setUpdateRulesNote(String value) {
        updateRulesNote = value;
    }

    /**
     * Triggers an update of the rule package list inside the P23R.
     *
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void updateRules() throws P23RAppFault_Exception {
        ruleActivate.updateNotificationRulePackages(updateRulesNote, tenant);
        RulePackageStateResultType result = ruleActivate.getNotificationRulePackageStates(tenant);
		controlCentres.clear();
		controlCentres.addAll(result.getControlCentres());
		rulePackageList.clear();
		rulePackageList.addAll(result.getPackageStates());
		
		updatePackageList();
    }

    /**
     * Update package list.
     */
    public void updatePackageList() {
        if (!controlCentres.isEmpty()) {
            String url = controlCentres.get(0);
            if (url != null) {
                                
                NotificationRulePackageListArchive listArchive = depot.loadPackagesList(tenant, url);
                PackageList list = listArchive.getNotificationRulePackageListManifestHelper();

                String username = p23rXmlDbClientConfig.get(String.format(P23RXmlDbClientConfig.XQUERY_USERNAME, "default"), "p23r");
                String password = p23rXmlDbClientConfig.get(String.format(P23RXmlDbClientConfig.XQUERY_PASSWORD, "default"), "temp");

                packages.clear();
                for (RulePackageStateType state : rulePackageList) {
                    for (RulePackage rulePackage : list.getContent().getRulePackages()) {
                        if (rulePackage.getId().equals(state.getId())) {
                            String location = rulePackage.getRulePackageFile().getLocation();

                            // split name from rest...
                            String name = location.substring(location.lastIndexOf('/'));
                            String address = location.substring(0, location.lastIndexOf('/'));

                            ModelAndRuleDepot packageDepot = ModelAndRuleDepot.create(address, username, password);
                            NotificationRulePackageArchive archive = (NotificationRulePackageArchive) packageDepot.loadPackage(name);

                            packages.add(new RulePackageBean(state, archive));
                            propagateRuleInfo(archive);
                        }
                    }
                }
            } else {
                // error: url is null
                LOG.error("control centre url is null");
            }
        }
    }
    
    /**
     * Sets the notification rule package states.
     *
     * @param rulePackageSates the rule package sates
     * @return the list
     * @throws P23RAppFault the p23 r app fault
     */
    protected List<RulePackageStateType> setNotificationRulePackageStates(
            List<RulePackageStateType> rulePackageSates) throws P23RAppFault_Exception {
    	List<RulePackageStateType> result = ruleActivate.setNotificationRulePackageStates(rulePackageSates, tenant, "");
		rulePackageList.clear();
		rulePackageList.addAll(result);
    	
        return rulePackageList;
    }

}
