package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;

/**
 * The Class RulePackageManager.
 */
@Named
@SessionScoped
public class RulePackageManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8262468891477275382L;

    private final transient Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private RulePackageListManager rulePackageListManager;

    private RulePackageBean rulePackage;

    /**
     * Gets the rule package.
     *
     * @return the rule package
     */
    public RulePackageBean getRulePackage() {
        return rulePackage;
    }

    /**
     * View package.
     *
     * @param rulePackage the rule package
     * @return the string
     */
    public String viewPackage(RulePackageBean rulePackage) {
        this.rulePackage = rulePackage;
        return "/secured/package?faces-redirect=true";
    }

    /**
     * Unselects a rulepackage in MARM.
     *
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void deselectPackage() throws P23RAppFault_Exception {
        log.debug("deselect package with ID {}", rulePackage.getId());
        setPackageIsSelected(false);
    }

    /**
     * Selects a rulepackage in MARM.
     *
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void toggleSelection() throws P23RAppFault_Exception {
        setPackageIsSelected(!rulePackage.isSelected());
    }

    private void setPackageIsSelected(boolean selected) throws P23RAppFault_Exception {

        List<RulePackageStateType> states = rulePackageListManager.getRulePackageStates();

        for (RulePackageStateType packageState : states) {
            if (packageState.getId().equals(rulePackage.getId())) {
                packageState.setIsSelected(selected);
                break;
            }
        }

        states = rulePackageListManager.setNotificationRulePackageStates(states);

        for (RulePackageStateType packageState : states) {
            if (packageState.getId().equals(rulePackage.getId())) {
                rulePackage.setState(packageState);
                break;
            }
        }
    }
}
