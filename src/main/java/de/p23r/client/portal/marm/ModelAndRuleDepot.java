package de.p23r.client.portal.marm;

import java.io.InputStream;
import java.io.Serializable;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import de.p23r.common.modelandrulemanagement.archive.DefaultArchive;
import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageListArchive;

/**
 * The Class ModelAndRuleDepot.
 */
@SuppressWarnings("deprecation")
public class ModelAndRuleDepot implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3093489154943183661L;

    /**
     * Creates the.
     *
     * @param address the address
     * @param username the username
     * @param password the password
     * @return the model and rule depot
     */
    public static ModelAndRuleDepot create(String address, String username, String password) {
    	ModelAndRuleDepot depot = new ModelAndRuleDepot();
    	depot.init(address, username, password);
    	return depot;
    }
    
    private ModelAndRuleDepotRest restClient;

    private void init(String address, String username, String password) {

        DefaultHttpClient httpClient = new DefaultHttpClient();
        
        if (username != null) {
            Credentials credentials = new UsernamePasswordCredentials(username, password);
            httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);            
        }

        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
        
        restClient = ProxyFactory.create(ModelAndRuleDepotRest.class, address, clientExecutor);
        
//        ClientConnectionManager cm = new PoolingClientConnectionManager();
//        DefaultHttpClient httpClient = new DefaultHttpClient(cm);
//        Credentials credentials = new UsernamePasswordCredentials(username, password);
//        httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, credentials);
//        
//        ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(httpClient);
//
//        ResteasyClient client = new ResteasyClientBuilder().httpEngine(engine).build();
//        ResteasyWebTarget target = client.target(address);
//        restClient = target.proxy(ModelAndRuleDepotRest.class);
                
    }
    
    /**
     * Loads the package list.
     *
     * @param tenant the tenant
     * @param address the address
     * @return the notification rule package list archive
     */
    public NotificationRulePackageListArchive loadPackagesList(String tenant, String address) {
        InputStream stream = restClient.getActualPackageList();
        return stream != null ? (NotificationRulePackageListArchive)DefaultArchive.createFromContent(stream) : null;
    }

    /**
     * Load package from uri.
     *
     * @param name the name
     * @return the default archive
     */
    public DefaultArchive loadPackage(String name) {
        InputStream stream = restClient.getPackage(name);
        return stream != null ? DefaultArchive.createFromContent(stream) : null;
    }

}
