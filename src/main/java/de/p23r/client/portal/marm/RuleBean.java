package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleStateType;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;
import de.p23r.leitstelle.ns.p23r.nrl.rulemanifest1_1.RuleManifest;

/**
 * The Class RuleBean.
 */
public class RuleBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 656373669545473852L;

    private RuleStateType state;
    private NotificationRulePackageArchive archive;
    private RuleManifest manifest;
    private String path;

    /**
     * Instantiates a new rule bean.
     *
     * @param state the state
     * @param archive the archive
     * @param groupPath the group path
     */
    public RuleBean(RuleStateType state, NotificationRulePackageArchive archive, String groupPath) {
        this.state = state;
        this.archive = archive;

        List<String> rulePaths = this.archive.getNotificationRulePathes(groupPath);
        for (String rulePath : rulePaths) {
            RuleManifest ruleManifest = this.archive.getNotificationRuleManifestHelper(rulePath);
            if (state.getId().equals(ruleManifest.getId())) {
                manifest = ruleManifest;
                path = rulePath;
            }
        }
    }

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    public void setState(RuleStateType state) {
        this.state = state;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return manifest.getId();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return manifest.getName();
    }

    /**
     * Gets the release.
     *
     * @return the release
     */
    public String getRelease() {
        return manifest.getRelease();
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return manifest.getTitle();
    }

    /**
     * Checks if is active.
     *
     * @return true, if is active
     */
    public boolean isActive() {
        return state.isIsActive();
    }

    /**
     * Checks if is deactivated.
     *
     * @return true, if is deactivated
     */
    public boolean isDeactivated() {
        return state.isIsDeactivated();
    }

    /**
     * Gets the descriptions.
     *
     * @return the descriptions
     */
    public List<Description> getDescriptions() {
        return manifest.getDescriptions();
    }

    /**
     * Gets the hints.
     *
     * @return the hints
     */
    public List<Description> getHints() {
        return manifest.getHints();
    }

    /**
     * Gets the targets.
     *
     * @return the targets
     */
    public List<Description> getTargets() {
        return manifest.getTargets();
    }

    /**
     * Gets the legal backgrounds.
     *
     * @return the legal backgrounds
     */
    public List<Description> getLegalBackgrounds() {
        return manifest.getLegalBackgrounds();
    }

    /**
     * Gets the creators.
     *
     * @return the creators
     */
    public List<String> getCreators() {
        return manifest.getCreators();
    }

    /**
     * Gets the subjects.
     *
     * @return the subjects
     */
    public List<String> getSubjects() {
        return manifest.getSubjects();
    }

    /**
     * Gets the warnings.
     *
     * @return the warnings
     */
    public List<String> getWarnings() {
        return state.getWarnings();
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return state.getErrors();
    }

    /**
     * Gets the submit at daily periods.
     *
     * @return the submit at daily periods
     */
    public List<XMLGregorianCalendar> getSubmitAtDailyPeriods() {
        return manifest.getSubmitAtDailyPeriods();
    }

    /**
     * Gets the submit at weekly periods.
     *
     * @return the submit at weekly periods
     */
    public List<Integer> getSubmitAtWeeklyPeriods() {
        return manifest.getSubmitAtWeeklyPeriods();
    }

    /**
     * Gets the submit at monthly periods.
     *
     * @return the submit at monthly periods
     */
    public List<XMLGregorianCalendar> getSubmitAtMonthlyPeriods() {
        return manifest.getSubmitAtMonthlyPeriods();
    }

    /**
     * Gets the submit at yearly periods.
     *
     * @return the submit at yearly periods
     */
    public List<XMLGregorianCalendar> getSubmitAtYearlyPeriods() {
        return manifest.getSubmitAtYearlyPeriods();
    }

    /**
     * Gets the submit ats.
     *
     * @return the submit ats
     */
    public List<XMLGregorianCalendar> getSubmitAts() {
        return manifest.getSubmitAts();
    }

    /**
     * Gets the message schema.
     *
     * @return the message schema
     */
    public String getMessageSchema() {
        return archive.getNotificationRuleMessageSchema(path);
    }

    /**
     * Gets the valid from.
     *
     * @return the valid from
     */
    public XMLGregorianCalendar getValidFrom() {
        return manifest.getValidFrom();
    }

    /**
     * Gets the valid until.
     *
     * @return the valid until
     */
    public XMLGregorianCalendar getValidUntil() {
        return manifest.getValidUntil();
    }

}
