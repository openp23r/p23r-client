package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.p23r.common.modelandrulemanagement.archive.NotificationRulePackageArchive;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;
import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;
import de.p23r.leitstelle.ns.p23r.nrl.rulepackagemanifest1_1.RulePackageManifest;

/**
 * The Class RulePackageBean.
 */
public class RulePackageBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -2042483246498558164L;

    private RulePackageStateType state;
    private NotificationRulePackageArchive archive;
    private RulePackageManifest manifest;

    /**
     * Instantiates a new rule package bean.
     *
     * @param state the state
     * @param archive the archive
     */
    public RulePackageBean(RulePackageStateType state, NotificationRulePackageArchive archive) {
        this.state = state;
        this.archive = archive;
        manifest = this.archive.getNotificationRulePackageManifestHelper();
    }

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    public void setState(RulePackageStateType state) {
        this.state = state;
    }

    /**
     * Checks if is supported.
     *
     * @return true, if is supported
     */
    public boolean isSupported() {
        return state.isIsSupported();
    }

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
        return state.isIsSelected();
    }

    /**
     * Checks if is recommended.
     *
     * @return true, if is recommended
     */
    public boolean isRecommended() {
        return state.isIsRecommended();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return manifest.getId();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return manifest.getName();
    }

    /**
     * Gets the release.
     *
     * @return the release
     */
    public String getRelease() {
        return manifest.getRelease();
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return manifest.getTitle();
    }

    /**
     * Gets the descriptions.
     *
     * @return the descriptions
     */
    public List<Description> getDescriptions() {
        return manifest.getDescriptions();
    }

    /**
     * Gets the hints.
     *
     * @return the hints
     */
    public List<Description> getHints() {
        return manifest.getHints();
    }

    /**
     * Gets the targets.
     *
     * @return the targets
     */
    public List<Description> getTargets() {
        return manifest.getTargets();
    }

    /**
     * Gets the licenses.
     *
     * @return the licenses
     */
    public List<Description> getLicenses() {
        return manifest.getLicenses();
    }

    /**
     * Gets the publisher.
     *
     * @return the publisher
     */
    public String getPublisher() {
        return manifest.getPublisher();
    }

    /**
     * Gets the creators.
     *
     * @return the creators
     */
    public List<String> getCreators() {
        return manifest.getCreators();
    }

    /**
     * Gets the subjects.
     *
     * @return the subjects
     */
    public List<String> getSubjects() {
        return manifest.getSubjects();
    }

    /**
     * Gets the warnings.
     *
     * @return the warnings
     */
    public List<String> getWarnings() {
        return state.getWarnings();
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return state.getErrors();
    }

    /**
     * Gets the rule groups.
     *
     * @return the rule groups
     */
    public List<RuleGroupBean> getRuleGroups() {
        List<RuleGroupBean> groups = new ArrayList<RuleGroupBean>();
        for (RuleGroupStateType groupState : state.getRuleGroups()) {
            groups.add(new RuleGroupBean(groupState, archive));
        }
        return groups;
    }

}
