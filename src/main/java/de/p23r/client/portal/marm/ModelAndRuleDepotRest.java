package de.p23r.client.portal.marm;

import java.io.InputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * The Interface ModelAndRuleDepotRest.
 */
public interface ModelAndRuleDepotRest {

    /**
     * Gets the actual package list.
     *
     * @return the actual package list
     */
    @GET
    @Path("/packages.lst")
    @Produces("application/zip")
    InputStream getActualPackageList();
    
    /**
     * Gets the package.
     *
     * @param packageName the package name
     * @return the package
     */
    @GET
    @Path("/{package}")
    @Produces("application/zip")
    InputStream getPackage(@PathParam("package") String packageName);
    
}
