package de.p23r.client.portal.marm;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RuleGroupStateType;
import de.p23r.leitstelle.ns.p23r.iruleactivate1_0.types.RulePackageStateType;

/**
 * The Class RuleGroupManager.
 */
@Named
@SessionScoped
public class RuleGroupManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1467816185019080138L;

    @Inject
    private RulePackageListManager rulePackageListManager;

    private RuleGroupBean ruleGroup;

    /**
     * Gets the rule group.
     *
     * @return the rule group
     */
    public RuleGroupBean getRuleGroup() {
        return ruleGroup;
    }

    /**
     * View rule group.
     *
     * @param ruleGroup the rule group
     * @return the string
     */
    public String viewRuleGroup(RuleGroupBean ruleGroup) {
        this.ruleGroup = ruleGroup;
        return "/secured/packagegroup?faces-redirect=true";
    }

    /**
     * Activates a rulegroup in MARM.
     *
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void toggleActivation() throws P23RAppFault_Exception {
        setGroupIsActivated(!ruleGroup.isActivated());
    }

    private void setGroupIsActivated(boolean activated) throws P23RAppFault_Exception {

        List<RulePackageStateType> states = rulePackageListManager.getRulePackageStates();

        for (RulePackageStateType packageState : states) {
            for (RuleGroupStateType groupState : packageState.getRuleGroups()) {
                if (groupState.getId().equals(ruleGroup.getId())) {
                    groupState.setIsActivated(activated);
                    break;
                }
            }
        }

        states = rulePackageListManager.setNotificationRulePackageStates(states);

        for (RulePackageStateType packageState : states) {
            for (RuleGroupStateType groupState : packageState.getRuleGroups()) {
                if (groupState.getId().equals(ruleGroup.getId())) {
                    ruleGroup.setState(groupState);
                    break;
                }
            }
        }
    }

}
