package de.p23r.client.portal.notification;

import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;

import de.p23r.common.existdb.P23RXmlDbClientConfig;
import de.p23r.common.existdb.P23RXmlDbResource;
import de.p23r.common.existdb.P23RXmlDbResourceContentAccess;
import de.p23r.client.ns.notifications.Notification;
import de.p23r.client.ns.notifications.Notifications;
import de.p23r.client.ns.ruleinfos.RuleInfo;
import de.p23r.client.ns.ruleinfos.RuleInfos;

/**
 * The Class XmlDbManager.
 */
@Named
@ApplicationScoped
public class XmlDbManager implements Serializable {

    private static final String DECLARE_DEFAULT_NAMESPACE_RULEINFOS = "declare default element namespace \"http://client.p23r.de/ns/ruleinfos\";";

    private static final String DECLARE_DEFAULT_NAMESPACE_NOTIFICATIONS = "declare default element namespace \"http://client.p23r.de/ns/notifications\";";

    /**
     * 
     */
    private static final long serialVersionUID = 5652037851450409462L;

    private static final transient Logger LOG = LoggerFactory.getLogger(XmlDbManager.class);

    private static final MessageFormat XQUERY_NOTIFICATIONINFO_UPDATE = new MessageFormat(
        DECLARE_DEFAULT_NAMESPACE_NOTIFICATIONS
                + "if (exists(//notification[notificationId=\"{0}\"][notificationRuleId=\"{1}\"])) "
                + "then update replace //notification[notificationId=\"{0}\"][notificationRuleId=\"{1}\"] with {2} "
                + "else update insert {2} into /notifications");

    private static final MessageFormat XQUERY_NOTIFICATIONINFO_REMOVE = new MessageFormat(
        DECLARE_DEFAULT_NAMESPACE_NOTIFICATIONS
                + "for $notification in //notification[notificationId=\"{0}\"][notificationRuleId=\"{1}\"] "
                + "return update delete $notification");

    private static final MessageFormat XQUERY_NOTIFICATIONINFO_QUERY = new MessageFormat(
        DECLARE_DEFAULT_NAMESPACE_NOTIFICATIONS
                + "//notification[notificationId=\"{0}\"][notificationRuleId=\"{1}\"]");

    private static final MessageFormat XQUERY_RULEINFO_UPDATE = new MessageFormat(
        DECLARE_DEFAULT_NAMESPACE_RULEINFOS + "if (exists(//ruleInfo[ruleID=\"{0}\"])) "
                + "then update replace //ruleInfo[ruleID=\"{0}\"] with {1} "
                + "else update insert {1} into /ruleInfos");

    private static final MessageFormat XQUERY_RULEINFO_QUERY = new MessageFormat(
        DECLARE_DEFAULT_NAMESPACE_RULEINFOS + "//ruleInfo[ruleID=\"{0}\"]");

    @Inject
    private P23RXmlDbClientConfig p23rXmlDbClientConfig;

    private P23RXmlDbResource notifications;

    private P23RXmlDbResource ruleinfos;

    private transient JAXBContext njc;
    private transient JAXBContext rjc;

    /**
     * Inits the {@link XmlDbManager}.
     */
    @PostConstruct
    public void init() {
        try {
            njc = JAXBContext.newInstance(Notifications.class, Notification.class);
            rjc = JAXBContext.newInstance(RuleInfos.class, RuleInfo.class);
        } catch (JAXBException e) {
            LOG.error("creating JAXB context failed", e);
        }

        notifications = p23rXmlDbClientConfig.getResource("/client/notifications/notifications.xml", true);
        ruleinfos = p23rXmlDbClientConfig.getResource("/client/ruleinfos.xml", true);
    }

    /**
     * Destroy the notifications and ruleinfos.
     */
    @PreDestroy
    public void destroy() {
        if (notifications != null) {
            notifications.free();
        }
        if (ruleinfos != null) {
            ruleinfos.free();
        }
    }

    /**
     * Gets the notifications.
     *
     * @return the notifications
     */
    public Notifications getNotifications() {
        return unmarshalNotifications();
    }

    /**
     * Store notification.
     *
     * @param notificationXML the notification xml
     * @param notificationId the notification id
     * @param notificationRuleId the notification rule id
     */
    public void storeNotification(String notificationXML, String notificationId, String notificationRuleId) {

        String documentName = notificationRuleId + "_" + notificationId + ".xml";
        P23RXmlDbResource notification = new P23RXmlDbResource(documentName,
            notifications.getParentCollection(), false);

        Notification notificationInfo = getNotificationInfo(notificationId, notificationRuleId);
        notificationInfo.setDocumentName(documentName);
        notificationInfo.setIsNew(false);
        notificationInfo.setIsModified(notification.exists());

        notification.setContent(notificationXML);

        updateNotificationInfo(notificationInfo);

        notification.free();
    }

    /**
     * Notification exist.
     *
     * @param notificationId the notification id
     * @param notificationRuleId the notification rule id
     * @return true, if successful
     */
    public boolean notificationExist(String notificationId, String notificationRuleId) {
        String documentName = notificationRuleId + "_" + notificationId + ".xml";
        P23RXmlDbResource notification = new P23RXmlDbResource(documentName,
            notifications.getParentCollection(), false);

        boolean exists = notification.exists();
        notification.free();
        return exists;
    }

    /**
     * Gets the notification.
     *
     * @param notificationId the notification id
     * @param notificationRuleId the notification rule id
     * @return the notification
     */
    public String getNotification(String notificationId, String notificationRuleId) {

        String id = notificationRuleId + "_" + notificationId + ".xml";
        P23RXmlDbResource notification = new P23RXmlDbResource(id, notifications.getParentCollection(), false);

        String content = P23RXmlDbResourceContentAccess.getContentAsString(notification);
        notification.free();
        return content;
    }

    /**
     * Update notification info.
     *
     * @param notification the notification
     */
    public void updateNotificationInfo(Notification notification) {

        String notificationInfo = marshalNotificationInfo(notification);

        String xupdate = XQUERY_NOTIFICATIONINFO_UPDATE.format(new Object[] {
                notification.getNotificationId(), notification.getNotificationRuleId(), notificationInfo });
        notifications.query(xupdate);
    }

    /**
     * Gets the notification info.
     *
     * @param notificationId the notification id
     * @param notificationRuleId the notification rule id
     * @return the notification info
     */
    public Notification getNotificationInfo(String notificationId, String notificationRuleId) {

        String xquery = XQUERY_NOTIFICATIONINFO_QUERY.format(new Object[] { notificationId,
                notificationRuleId });
        ResourceSet result = notifications.query(xquery);

        try {
            if (result != null && result.getSize() > 0) {
                String notificationInfo = result.getResource(0).getContent().toString();
                return unmarshalNotificationInfo(notificationInfo);
            }
        } catch (XMLDBException e) {
            LOG.error("Failed to get notification info.", e);
        }
        return null;
    }

    /**
     * Gets the notification infos.
     *
     * @param xquery the xquery
     * @return the notification infos
     */
    public List<Notification> getNotificationInfos(String xquery) {
        List<Notification> notificationList = new ArrayList<Notification>();
        ResourceSet result = notifications.query(xquery);
        if (result != null) {
            try {
                ResourceIterator it = result.getIterator();
                while (it.hasMoreResources()) {
                    Resource resource = it.nextResource();
                    notificationList.add(unmarshalNotificationInfo(resource.getContent().toString()));
                }
            } catch (XMLDBException e) {
                LOG.error("iterating over notification query result failed", e);
            }
        }
        return notificationList;
    }

    /**
     * Removes the notification.
     *
     * @param notificationId the notification id
     * @param notificationRuleId the notification rule id
     */
    public void removeNotification(String notificationId, String notificationRuleId) {
        String xupdate = XQUERY_NOTIFICATIONINFO_REMOVE.format(new Object[] { notificationId,
                notificationRuleId });
        notifications.query(xupdate);

        String documentName = notificationRuleId + "_" + notificationId + ".xml";
        P23RXmlDbResource notification = new P23RXmlDbResource(documentName,
            notifications.getParentCollection(), false);
        notification.remove();
        notification.free();
    }

    /**
     * Gets the rule info.
     *
     * @param notificationRuleId the notification rule id
     * @return the rule info
     */
    public RuleInfo getRuleInfo(String notificationRuleId) {
        String xquery = XQUERY_RULEINFO_QUERY.format(new Object[] { notificationRuleId });
        ResourceSet result = ruleinfos.query(xquery);

        try {
            if (result != null && result.getSize() > 0) {
                String ruleInfo = result.getResource(0).getContent().toString();
                return unmarshalRuleInfo(ruleInfo);
            }
        } catch (XMLDBException e) {
            LOG.error("Failed to get rule info.", e);
        }
        return null;
    }

    /**
     * Update rule info.
     *
     * @param rule the rule
     */
    public void updateRuleInfo(RuleInfo rule) {

        String ruleInfo = marshalRuleInfo(rule);
        LOG.debug("update ruleinfo: {}", ruleInfo);
        String xupdate = XQUERY_RULEINFO_UPDATE.format(new Object[] { rule.getRuleID(), ruleInfo });
        ruleinfos.query(xupdate);
    }

    private String marshalNotificationInfo(Notification notification) {
        StringWriter writer = new StringWriter();
        try {
            Marshaller m = njc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            JAXBElement<Notification> jaxbElement = new JAXBElement<Notification>(new QName(
                "http://client.p23r.de/ns/notifications", "notification"), Notification.class, notification);
            m.marshal(jaxbElement, writer);
        } catch (JAXBException e) {
            LOG.error("Failed to marshal notification.", e);
        }

        return writer.toString();
    }

    private Notification unmarshalNotificationInfo(String notificationInfo) {
        try {
            Unmarshaller um = njc.createUnmarshaller();
            return (Notification) um.unmarshal(new StringReader(notificationInfo));
        } catch (JAXBException e) {
            LOG.error("Failed to unmarshal notification info.", e);
        }

        return null;
    }

    private Notifications unmarshalNotifications() {
        String content = P23RXmlDbResourceContentAccess.getContentAsString(notifications);

        try {
            Unmarshaller um = njc.createUnmarshaller();
            return (Notifications) um.unmarshal(new StringReader(content));
        } catch (JAXBException e) {
            LOG.error("Failed to unmarshal notifications.", e);
        }

        return null;
    }

    private String marshalRuleInfo(RuleInfo ruleInfo) {
        StringWriter writer = new StringWriter();
        try {
            Marshaller m = rjc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            JAXBElement<RuleInfo> jaxbElement = new JAXBElement<RuleInfo>(new QName(
                "http://client.p23r.de/ns/ruleinfos", "ruleInfo"), RuleInfo.class, ruleInfo);
            m.marshal(jaxbElement, writer);
        } catch (JAXBException e) {
            LOG.error("Failed to marshal rule info.", e);
        }

        return writer.toString();
    }

    private RuleInfo unmarshalRuleInfo(String ruleInfo) {
        try {
            Unmarshaller um = rjc.createUnmarshaller();
            return (RuleInfo) um.unmarshal(new StringReader(ruleInfo));
        } catch (JAXBException e) {
            LOG.error("Failed to unmarshal rule info.", e);
        }

        return null;
    }

}
