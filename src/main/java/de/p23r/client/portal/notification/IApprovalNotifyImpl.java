package de.p23r.client.portal.notification;

import java.util.GregorianCalendar;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.client.ns.notifications.Notification;
import de.p23r.client.portal.common.P23RFault;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.iapprovalnotify1_0.IApprovalNotify;

/**
 * The Class IApprovalNotifyImpl.
 */
@WebService(serviceName = "IApprovalNotify", endpointInterface = "de.p23r.leitstelle.ns.p23r.iapprovalnotify1_0.IApprovalNotify", targetNamespace = "http://leitstelle.p23r.de/NS/P23R/IApprovalNotify1-0")
public class IApprovalNotifyImpl implements IApprovalNotify {

    private static final String NM002 = "NM002";

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private XmlDbManager xmldbManager;

    // @Inject
    // private IdentityManager identityManager;
    //
    // @Inject
    // private NotificationAction notificationAction;

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.p23r.leitstelle.ns.p23r.iapprovalnotify1_0.IApprovalNotify#notifyApproval
     * (java.lang.String, java.lang.String)
     */
    @Override
    public void notifyApproval(String notificationId, String notificationRuleId)
            throws P23RAppFault_Exception {
        log.info("-> notifyApproval()");
        log.info("-  notificationId: {}", notificationId);
        log.info("-  notificationRuleId: {}", notificationRuleId);

        if (notificationId == null || notificationId.isEmpty()) {
            throw P23RFault.createP23RAppFault(
                    "missing parameter notificatonId", null, NM002);
        }
        if (notificationRuleId == null || notificationRuleId.isEmpty()) {
            throw P23RFault.createP23RAppFault(
                    "missing parameter notificatonRuleId", null, NM002);
        }

        // Store the notification in the database
        Notification notification = new Notification();
        notification.setIsExported(false);
        notification.setIsModified(false);
        notification.setIsNew(true);
        notification.setIsTransmitted(false);
        notification.setNotificationId(notificationId);
        notification.setNotificationRuleId(notificationRuleId);
        try {
            notification.setEntryDate(DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(new GregorianCalendar()));
        } catch (DatatypeConfigurationException e2) {
            log.error("Failed to set entry date in notify approval.", e2);
        }
        xmldbManager.updateNotificationInfo(notification);

        // RuleInfo ruleInfo = xmldbManager.getRuleInfo(notificationRuleId);
        // if (ruleInfo != null && ruleInfo.isAutoNotification()) {
        // List<P23RUser> users =
        // identityManager.createIdentityQuery(P23RUser.class).setParameter(P23RUser.LOGIN_NAME,
        // ruleInfo.isAutoNotification()).getResultList();
        // if (users != null && !users.isEmpty()) {
        // try {
        // notificationAction.approveTrue(notification, users.get(0));
        // } catch (P23RAppFault e) {
        // e.printStackTrace();
        // }
        // }
        // }

    }

}
