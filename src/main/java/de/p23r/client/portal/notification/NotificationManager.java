package de.p23r.client.portal.notification;

import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.picketlink.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.p23r.client.ns.notifications.Notification;
import de.p23r.client.ns.notifications.Notifications;
import de.p23r.client.portal.common.INotificationApproveHelper;

/**
 * The Class NotificationManager.
 */
@Named
@ViewScoped
public class NotificationManager {

    private static final String XQUERY_NOTIFICATIONINFO_QUERY_NEW = "declare default element namespace \"http://client.p23r.de/ns/notifications\"; //notification[isNew=\"true\"]";

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private XmlDbManager xmldbManager;

    @Inject
    private INotificationApproveHelper notificationApprove;

    @SuppressWarnings("unused")
    @Inject
    private Identity identity;

    /**
     * Gets the new reports.
     *
     * @return the new reports
     */
    public List<Notification> getNewReports() {
        List<Notification> notifications = xmldbManager
            .getNotificationInfos(XQUERY_NOTIFICATIONINFO_QUERY_NEW);
        return notifications;
    }

    /**
     * Find notifications.
     *
     * @return the reports
     */
    public List<Notification> getReports() {
        Notifications notifications = xmldbManager.getNotifications();
        return notifications.getNotification();
    }

    /**
     * Clean.
     */
    public void clean() {
        Notifications notifications = xmldbManager.getNotifications();
        for (Notification notification : notifications.getNotification()) {
            if (notificationApprove.getNotification(notification.getNotificationId()) == null) {
                log.debug("notification no more available");
                xmldbManager.removeNotification(notification.getNotificationId(),
                    notification.getNotificationRuleId());
            }
        }
    }

}
