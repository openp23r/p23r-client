package de.p23r.client.portal.notification;

import java.io.IOException;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.GregorianCalendar;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.io.IOUtils;
import org.picketlink.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3._2000._09.xmldsig.SignatureType;
import org.w3c.dom.Element;

import de.p23r.client.ns.notifications.Notification;
import de.p23r.client.portal.common.INotificationApproveHelper;
import de.p23r.client.portal.session.P23RKeyStoreException;
import de.p23r.client.portal.session.P23RUser;
import de.p23r.client.portal.session.Security;
import de.p23r.common.modelandrulemanagement.P23RSignature;
import de.p23r.common.modelandrulemanagement.TransformerHelper;
import de.p23r.leitstelle.ns.p23r.commonfaults1_0.P23RAppFault_Exception;
import de.p23r.leitstelle.ns.p23r.inotificationapprove1_0.types.Approval;

/**
 * Handles the notification approval.
 */
@Named
@SessionScoped
public class NotificationAction implements Serializable {

    private static final String DECLINED = "declined";

    private static final String APPROVED = "approved";

    /**
     * 
     */
    private static final long serialVersionUID = 4759223933130065098L;

    private static final transient Logger LOG = LoggerFactory.getLogger(NotificationAction.class);

    private static final String NOTIFICATION_URL = "/secured/notification?faces-redirect=true";

    private Notification currentNotification;

    @Inject
    private Identity identity;

    @Inject
    private XmlDbManager xmldbManager;

    @Inject
    private INotificationApproveHelper notificationApprove;

    private String approvalNote = "";
    private String notificationXML = "";

    /**
     * Returns the approval note.
     * 
     * @return the note
     */
    public String getApprovalNote() {
        return this.approvalNote;
    }

    /**
     * Sets the approval note.
     * 
     * @param approvalNote
     *            , the note
     */
    public void setApprovalNote(String approvalNote) {
        this.approvalNote = approvalNote;
    }

    /**
     * Triggered by UI. Loads the selected notification
     *
     * @param notification            to load
     * @return the string
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String viewNotification(Notification notification) throws P23RAppFault_Exception {
        LOG.debug("view notification " + notification.getNotificationId());

        fetchNotificationXML(notification);
        setCurrentNotification(notification);

        return NOTIFICATION_URL;
    }

    /**
     * Triggered by UI. Loads the selected notification
     *
     * @param notification            to load
     * @return the string
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String editNotification(Notification notification) throws P23RAppFault_Exception {
        LOG.debug("edit notification " + notification.getNotificationId());

        fetchNotificationXML(notification);
        setCurrentNotification(notification);

        return NOTIFICATION_URL;
    }

    /**
     * Returns the notification XML of the current selected notification.
     *
     * @param notification            the notification
     * @return the XML as String
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String fetchNotificationXML(Notification notification) throws P23RAppFault_Exception {
        if (xmldbManager.notificationExist(notification.getNotificationId(), notification.getNotificationRuleId())) {
            notificationXML = xmldbManager.getNotification(notification.getNotificationId(), notification.getNotificationRuleId());
        } else {
            fetchNotification(notification);
        }
        return notificationXML;
    }

    /**
     * Gets the notification xml.
     *
     * @return the notification xml
     */
    public String getNotificationXML() {
        return notificationXML;
    }

    /**
     * Sets the notification xml.
     *
     * @param notificationXML the new notification xml
     */
    public void setNotificationXML(String notificationXML) {
        this.notificationXML = notificationXML;
    }

    /**
     * Approve true.
     *
     * @return the string
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String approveTrue() throws P23RAppFault_Exception {
        P23RUser user = (P23RUser) identity.getAccount();
        approveTrue(currentNotification, user);
        return NOTIFICATION_URL;
    }

    /**
     * Approve false.
     *
     * @return the string
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String approveFalse() throws P23RAppFault_Exception {
        P23RUser user = (P23RUser) identity.getAccount();
        approveFalse(user);
        return NOTIFICATION_URL;
    }

    /**
     * Approves a notification.
     *
     * @param notification            the notification
     * @param user            the user
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public void approveTrue(Notification notification, P23RUser user) throws P23RAppFault_Exception {
        LOG.debug("notification {} approved", notification.getNotificationId());

        Approval approval = new Approval();
        approval.setApproved(APPROVED);
        approval.setNote(getApprovalNote());
        approval.setName(user.getLoginName());

        notificationXML = fetchNotificationXML(notification);

        Element notificationXMLElement = TransformerHelper.elementFromString(notificationXML);

        try {
            Security security = new Security();
            security.setCertificate(user.getCertificate());
            security.setPrivateKey(user.getPrivateKey());
            security.setPrivateKeyPassphrase(user.getPrivateKeyPassphrase());
            if (security.getCertificate() != null && security.getPrivateKey() != null) {
                Element signature = new P23RSignature().sign(notificationXMLElement, (X509Certificate) security.getCertificate(), security.getPrivateKey());
                if (signature != null) {
                    JAXBContext jaxbContext = JAXBContext.newInstance(SignatureType.class);
                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    JAXBElement<SignatureType> jaxbSignature = unmarshaller.unmarshal(signature, SignatureType.class);
                    approval.setSignature(jaxbSignature.getValue());
                    LOG.debug("signed element: {}", TransformerHelper.nodeToString(notificationXMLElement, false));
                }
            }
        } catch (JAXBException e) {
            LOG.warn("Could not marshal SignatureType", e);
        } catch (P23RKeyStoreException e) {
            LOG.warn("signing notification failed", e);
        }

        notification.setApprovalStatus(APPROVED);
        notificationApprove(approval, notification);
    }

    /**
     * Triggered by UI. declines a notification
     *
     * @param user the user
     * @return the string
     * @throws P23RAppFault_Exception the p23 r app fault_ exception
     */
    public String approveFalse(P23RUser user) throws P23RAppFault_Exception {
        LOG.debug("decline notification {}", currentNotification.getNotificationId());

        Approval approval = new Approval();
        approval.setApproved(DECLINED);
        approval.setNote(getApprovalNote());
        approval.setName(user.getLoginName());

        notificationXML = fetchNotificationXML(currentNotification);

        currentNotification.setApprovalStatus(DECLINED);
        notificationApprove(approval, currentNotification);

        return NOTIFICATION_URL;
    }

    private void notificationApprove(Approval approval, Notification notification) throws P23RAppFault_Exception {
        notificationApprove.approveNotification(approval, notificationXML, notification.getNotificationId());

        // Update the notification status
        notification.setIsNew(false);
        notification.setIsTransmitted(true);
        notification.setNote(approvalNote);
        try {
            notification.setTransmissionDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
        } catch (DatatypeConfigurationException e) {
            LOG.warn("could not set transmission date", e);
        }

        if (xmldbManager == null) {
            xmldbManager = new XmlDbManager();
        }

        xmldbManager.updateNotificationInfo(notification);
    }

    private String fetchNotification(Notification notification) throws P23RAppFault_Exception {
        de.p23r.leitstelle.ns.p23r.common1_0.Notification not = notificationApprove.getNotification(notification.getNotificationId());
        try {
        	notificationXML = IOUtils.toString(not.getContent().getInputStream(), "UTF-8");
//            notificationXML = TransformerHelper.stringFromSource(new StreamSource(not.getContent().getInputStream()));
        } catch (IOException e) {
            LOG.error("Failed to fetch notification with id {}.", notification.getNotificationId());
            throw new P23RAppFault_Exception("Failed to fetch notification with id " + notification.getNotificationId() + ".", e);
        }

        xmldbManager.storeNotification(notificationXML, notification.getNotificationId(), notification.getNotificationRuleId());
        return notificationXML;
    }

    /**
     * Gets the current notification.
     *
     * @return the current notification
     */
    public Notification getCurrentNotification() {
        return currentNotification;
    }

    /**
     * Sets the current notification.
     *
     * @param currentNotification the new current notification
     */
    public void setCurrentNotification(Notification currentNotification) {
        this.currentNotification = currentNotification;
        approvalNote = this.currentNotification.getNote();
    }

}
