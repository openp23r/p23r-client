package de.p23r.client.portal.converters;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

/**
 * Converts a List<String> into a comma seperated String.
 *
 * @author ara
 */
@FacesConverter("listStringConverter")
public class ListStringConverter implements javax.faces.convert.Converter {

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent ui, String arg) {
        return null;
    }

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent ui, Object object) {
        if (object == null) {
            return "";
        }

        if (object instanceof String) {
            return object.toString();
        }

        @SuppressWarnings("unchecked")
        List<String> list = (List<String>) object;

        StringBuilder sb = new StringBuilder();
        for (String x : list) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(x);
        }

        return sb.toString();
    }

}
