package de.p23r.client.portal.converters;

import java.util.List;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import de.p23r.leitstelle.ns.p23r.nrl.commonnrl1_1.Description;

/**
 * Converts a List<String> into a comma seperated String.
 *
 * @author ara
 */
@FacesConverter("languageSelectionConverter")
public class LanguageSelectionConverter implements javax.faces.convert.Converter {

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent ui, String arg) {
        return null;
    }

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent ui, Object object) {
        if (object == null) {
            return "";
        }

        if (object instanceof String) {
            return object.toString();
        }

        @SuppressWarnings("unchecked")
        List<Description> descriptions = (List<Description>) object;

        for (Description description : descriptions) {
            Locale loc = Locale.forLanguageTag(description.getLang());
            if (loc.equals(Locale.getDefault())) {
                return description.getContent();
            }
        }
        return descriptions.isEmpty() ? "" : descriptions.get(0).getContent();
    }

}
