package de.p23r.client.portal.converters;

import java.text.SimpleDateFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CalendarConverter.
 * 
 * @author sim
 */
@FacesConverter("calendarConverter")
public class CalendarConverter implements javax.faces.convert.Converter {

    private final Logger log = LoggerFactory.getLogger(getClass());

    /*
     * (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String calendar) {
        XMLGregorianCalendar xmlgregcal = null;
        if (calendar != null && !calendar.isEmpty()) {
            try {
                xmlgregcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            } catch (DatatypeConfigurationException e) {
                log.error("Failed to convert a String to an XMLGregorianCalendar.", e);
            }
        }
        return xmlgregcal;
    }

    /*
     * (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            XMLGregorianCalendar calendar = (XMLGregorianCalendar) object;
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            return format.format(calendar.toGregorianCalendar().getTime());
        }
        return null;
    }

}
