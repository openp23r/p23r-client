/**
 * Provides P23R user, authentication and login classes.
 */
package de.p23r.client.portal.session;