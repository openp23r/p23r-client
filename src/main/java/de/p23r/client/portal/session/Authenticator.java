package de.p23r.client.portal.session;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.credential.Credentials;
import org.picketlink.idm.credential.Credentials.Status;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.credential.UsernamePasswordCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@PicketLink
/**
 * The Class Authenticator.
 */
public class Authenticator extends BaseAuthenticator {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Inject
    private DefaultLoginCredentials defaultLoginCredentials;

    @Inject
    private IdentityManager identityManager;

    private static final String UNKNOWN_USER = "Unbekannter Benutzer!";

    private static final String WRONG_PASSWORD = "Falsches Passwort!";

    /* (non-Javadoc)
     * @see org.picketlink.authentication.Authenticator#authenticate()
     */
    @Override
    public void authenticate() {
        String username = defaultLoginCredentials.getUserId();
        log.debug("authenticating user {}...", username);

        List<P23RUser> users = identityManager.createIdentityQuery(P23RUser.class).setParameter(P23RUser.LOGIN_NAME, defaultLoginCredentials.getUserId())
            .getResultList();
        if (users == null || users.isEmpty()) {
            setStatus(AuthenticationStatus.FAILURE);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, UNKNOWN_USER, UNKNOWN_USER));
            return;
        }

        Credentials creds = new UsernamePasswordCredentials(defaultLoginCredentials.getUserId(), new Password(defaultLoginCredentials.getPassword()));
        identityManager.validateCredentials(creds);
        if (Status.VALID.equals(creds.getStatus())) {
            setStatus(AuthenticationStatus.SUCCESS);
            setAccount(users.get(0));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, WRONG_PASSWORD, WRONG_PASSWORD));
            setStatus(AuthenticationStatus.FAILURE);
        }

//        WSTrustClient client = new WSTrustClient("PicketLinkSTS", "PicketLinkSTSPort", "http://localhost:8080/picketlink-sts/PicketLinkSTS", new SecurityInfo(username, defaultLoginCredentials.getPassword()));
//        Element assertion = null;
//        try {
//            log.debug("Invoking token service to get SAML assertion for {}", username);
//            assertion = client.issueToken(SAMLUtil.SAML2_TOKEN_TYPE);
//            log.debug("SAML assertion for {} successfully obtained!", username);
//        } catch (WSTrustException wse) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falsches Passwort!", "Falsches Passwort!"));
//            setStatus(AuthenticationStatus.FAILURE);
//            System.out.println("Unable to issue assertion: " + wse.getMessage());
//            wse.printStackTrace();
//        }

    }

}
