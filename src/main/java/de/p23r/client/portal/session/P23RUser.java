package de.p23r.client.portal.session;

import org.picketlink.idm.model.annotation.AttributeProperty;
import org.picketlink.idm.model.basic.User;

/**
 * The Class P23RUser.
 */
public class P23RUser extends User {

    /**
     * 
     */
    private static final long serialVersionUID = 3607931949449587691L;

    @AttributeProperty
    private byte[] certificate;

    @AttributeProperty
    private byte[] privateKey;

    @AttributeProperty
    private String privateKeyPassphrase;

    @AttributeProperty
    private String organisationUnit;

    /**
     * Instantiates a new p23r user.
     */
    public P23RUser() {
    }

    /**
     * Instantiates a new p23r user.
     *
     * @param loginName the login name
     */
    public P23RUser(String loginName) {
        super(loginName);
    }

    /**
     * Gets the certificate.
     *
     * @return the certificate
     */
    public byte[] getCertificate() {
        return certificate.clone();
    }

    /**
     * Sets the certificate.
     *
     * @param certificate the new certificate
     */
    public void setCertificate(byte[] certificate) {
        this.certificate = certificate.clone();
    }

    /**
     * Gets the private key.
     *
     * @return the private key
     */
    public byte[] getPrivateKey() {
        return privateKey.clone();
    }

    /**
     * Sets the private key.
     *
     * @param privateKey the new private key
     */
    public void setPrivateKey(byte[] privateKey) {
        this.privateKey = privateKey.clone();
    }

    /**
     * Gets the private key passphrase.
     *
     * @return the private key passphrase
     */
    public String getPrivateKeyPassphrase() {
        return privateKeyPassphrase;
    }

    /**
     * Sets the private key passphrase.
     *
     * @param privateKeyPassphrase the new private key passphrase
     */
    public void setPrivateKeyPassphrase(String privateKeyPassphrase) {
        this.privateKeyPassphrase = privateKeyPassphrase;
    }

    /**
     * Gets the organisation unit.
     *
     * @return the organisation unit
     */
    public String getOrganisationUnit() {
        return organisationUnit;
    }

    /**
     * Sets the organisation unit.
     *
     * @param organisationUnit the new organisation unit
     */
    public void setOrganisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

}
