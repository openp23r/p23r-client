package de.p23r.client.portal.session;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LoginAction.
 */
@Named
@RequestScoped
public class LoginAction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7332232621290392397L;

    private static final transient Logger LOG = LoggerFactory.getLogger(LoginAction.class);

    @Inject
    private Identity identity;

    private String from;

    /**
     * Login.
     */
    public void login() {
        if (AuthenticationResult.SUCCESS.equals(identity.login())) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(from);
            } catch (IOException e) {
                LOG.error("Login failed.", e);
            }
        }
    }

    /**
     * Gets the from.
     *
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the from.
     *
     * @param from the new from
     */
    public void setFrom(String from) {
        this.from = from;
    }

}
