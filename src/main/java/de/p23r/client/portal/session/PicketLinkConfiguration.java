package de.p23r.client.portal.session;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import org.picketlink.IdentityConfigurationEvent;
import org.picketlink.idm.config.IdentityConfigurationBuilder;

/**
 * The Class PicketLinkConfiguration.
 */
@ApplicationScoped
public class PicketLinkConfiguration {

    /**
     * Observe identity configuration event.
     *
     * @param event the event
     */
    public void observeIdentityConfigurationEvent(@Observes IdentityConfigurationEvent event) {
        IdentityConfigurationBuilder builder = event.getConfig();
        builder.named("default").stores().file()
            .addCredentialHandler(P23RUserPasswordCredentialHandler.class).supportAllFeatures();
        builder.buildAll();
    }

}
