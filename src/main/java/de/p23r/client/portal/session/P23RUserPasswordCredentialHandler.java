package de.p23r.client.portal.session;

import java.util.List;

import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.config.IdentityStoreConfiguration;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.credential.UsernamePasswordCredentials;
import org.picketlink.idm.credential.handler.PasswordCredentialHandler;
import org.picketlink.idm.model.Account;
import org.picketlink.idm.query.IdentityQuery;
import org.picketlink.idm.spi.CredentialStore;
import org.picketlink.idm.spi.IdentityContext;

/**
 * The Class P23RUserPasswordCredentialHandler.
 */
public class P23RUserPasswordCredentialHandler
        extends
        PasswordCredentialHandler<CredentialStore<IdentityStoreConfiguration>, UsernamePasswordCredentials, Password> {

    /* (non-Javadoc)
     * @see org.picketlink.idm.credential.handler.PasswordCredentialHandler#getAccount(org.picketlink.idm.spi.IdentityContext, org.picketlink.idm.credential.UsernamePasswordCredentials)
     */
    @Override
    protected Account
            getAccount(final IdentityContext context, final UsernamePasswordCredentials credentials) {
        IdentityManager identityManager = getIdentityManager(context);
        IdentityQuery<P23RUser> query = identityManager.createIdentityQuery(P23RUser.class);
        query.setParameter(P23RUser.LOGIN_NAME, credentials.getUsername());

        List<P23RUser> result = query.getResultList();
        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

}
