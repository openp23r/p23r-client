package de.p23r.client.portal.session;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Security.
 */
public class Security {

    private final Logger log = LoggerFactory.getLogger(Security.class);

    private static final String SHA1_WITH = "SHA1with";

    private byte[] privateKey;

    /**
     * Sets the privateKey as byteArray.
     * 
     * @param privateKey the new private key
     */
    public void setPrivateKey(byte[] privateKey) {
        this.privateKey = privateKey.clone();
    }

    private byte[] certificate;

    /**
     * Sets the certificate as byteArray.
     * 
     * @param certificate the new certificate
     */
    public void setCertificate(byte[] certificate) {
        this.certificate = certificate.clone();
    }

    private String privateKeyPassphrase;

    /**
     * Sets the privateKey passphrase.
     * 
     * @param passphrase the new private key passphrase
     */
    public void setPrivateKeyPassphrase(String passphrase) {
        this.privateKeyPassphrase = passphrase;
    }

    /**
     * Get the privateKey of the privateKey datefile.
     * 
     * @return PrivateKey, null if no PrivateKey available
     * @throws P23RKeyStoreException the keystore exception
     */
    public PrivateKey getPrivateKey() throws P23RKeyStoreException {
        if (privateKey == null) {
            return null;
        }

        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(new ByteArrayInputStream(privateKey), privateKeyPassphrase.toCharArray());
            return (PrivateKey) keystore.getKey(keystore.aliases().nextElement(),
                privateKeyPassphrase.toCharArray());
        } catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException
                | CertificateException | IOException e) {
            log.debug("get private key", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        }
    }

    /**
     * Get the certificate of the certificate datafile.
     * 
     * @return Certificate, null if no certificate available
     * @throws P23RKeyStoreException the keystore exception
     */
    public Certificate getCertificate() throws P23RKeyStoreException {
        if (certificate == null) {
            return null;
        }
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            return cf.generateCertificate(new ByteArrayInputStream(certificate));
        } catch (CertificateException e) {
            log.debug("get certificate", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        }
    }

    /**
     * Get the PublicKey of the certificate datafile.
     * 
     * @return PublicKey, null if no certificate available
     * @throws P23RKeyStoreException the keystore exception
     */
    public PublicKey getPublicKey() throws P23RKeyStoreException {
        Certificate cert = getCertificate();
        if (cert == null) {
            return null;
        }
        return cert.getPublicKey();
    }

    /**
     * Signs a bytearray with a given private key.
     * 
     * @param data the data
     * @param key the key
     * @return byte[], the signature
     * @throws P23RKeyStoreException the keystore exception
     */
    public byte[] signData(byte[] data, PrivateKey key) throws P23RKeyStoreException {
        try {
            String algorithm = SHA1_WITH + key.getAlgorithm();
            Signature signer = Signature.getInstance(algorithm);
            signer.initSign(key);
            signer.update(data);
            return signer.sign();
        } catch (SignatureException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.debug("sign data", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        }
    }

    /**
     * Verifies a signature.
     * 
     * @param data the data
     * @param key the key
     * @param sig the sig
     * @return true if signature is valid, otherwise false
     * @throws P23RKeyStoreException the keystore exception
     */
    public boolean verifySig(byte[] data, PublicKey key, byte[] sig) throws P23RKeyStoreException {
        try {
            String algorithm = SHA1_WITH + key.getAlgorithm();
            Signature signer = Signature.getInstance(algorithm);
            signer.initVerify(key);
            signer.update(data);
            return signer.verify(sig);
        } catch (SignatureException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.debug("verify signature", e);
            throw new P23RKeyStoreException(e.getMessage(), e);
        }
    }

}
