package de.p23r.client.portal.session;

/**
 * The Class P23RKeyStoreException.
 */
public class P23RKeyStoreException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -5825322718460229640L;

    /**
     * Instantiates a new key store exception.
     */
    public P23RKeyStoreException() {
        super();
    }

    /**
     * Instantiates a new key store exception.
     * 
     * @param message the message
     * @param cause the cause
     */
    public P23RKeyStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new key store exception.
     * 
     * @param message the message
     */
    public P23RKeyStoreException(String message) {
        super(message);
    }

    /**
     * Instantiates a new key store exception.
     * 
     * @param cause the cause
     */
    public P23RKeyStoreException(Throwable cause) {
        super(cause);
    }
}
