//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.02.13 um 11:15:14 AM CET 
//


package de.p23r.client.ns.notifications;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}entryDate"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}notificationId"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}notificationRuleId"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}documentName"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}originalDocumentName"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}isNew"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}isModified"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}isExported"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}isTransmitted"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}transmissionDate" minOccurs="0"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}approvalStatus" minOccurs="0"/>
 *         &lt;element ref="{http://client.p23r.de/ns/notifications}note" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "entryDate",
    "notificationId",
    "notificationRuleId",
    "documentName",
    "originalDocumentName",
    "isNew",
    "isModified",
    "isExported",
    "isTransmitted",
    "transmissionDate",
    "approvalStatus",
    "note"
})
@XmlRootElement(name = "notification")
public class Notification
    implements Serializable
{

    private final static long serialVersionUID = 12343L;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar entryDate;
    @XmlElement(required = true)
    protected String notificationId;
    @XmlElement(required = true)
    protected String notificationRuleId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String documentName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String originalDocumentName;
    @XmlElement(defaultValue = "true")
    protected boolean isNew;
    @XmlElement(defaultValue = "false")
    protected boolean isModified;
    @XmlElement(defaultValue = "false")
    protected boolean isExported;
    @XmlElement(defaultValue = "false")
    protected boolean isTransmitted;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transmissionDate;
    protected String approvalStatus;
    protected String note;

    /**
     * Ruft den Wert der entryDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEntryDate() {
        return entryDate;
    }

    /**
     * Legt den Wert der entryDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEntryDate(XMLGregorianCalendar value) {
        this.entryDate = value;
    }

    /**
     * Ruft den Wert der notificationId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationId() {
        return notificationId;
    }

    /**
     * Legt den Wert der notificationId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationId(String value) {
        this.notificationId = value;
    }

    /**
     * Ruft den Wert der notificationRuleId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationRuleId() {
        return notificationRuleId;
    }

    /**
     * Legt den Wert der notificationRuleId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationRuleId(String value) {
        this.notificationRuleId = value;
    }

    /**
     * Ruft den Wert der documentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * Legt den Wert der documentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentName(String value) {
        this.documentName = value;
    }

    /**
     * Ruft den Wert der originalDocumentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalDocumentName() {
        return originalDocumentName;
    }

    /**
     * Legt den Wert der originalDocumentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalDocumentName(String value) {
        this.originalDocumentName = value;
    }

    /**
     * Ruft den Wert der isNew-Eigenschaft ab.
     * 
     */
    public boolean isIsNew() {
        return isNew;
    }

    /**
     * Legt den Wert der isNew-Eigenschaft fest.
     * 
     */
    public void setIsNew(boolean value) {
        this.isNew = value;
    }

    /**
     * Ruft den Wert der isModified-Eigenschaft ab.
     * 
     */
    public boolean isIsModified() {
        return isModified;
    }

    /**
     * Legt den Wert der isModified-Eigenschaft fest.
     * 
     */
    public void setIsModified(boolean value) {
        this.isModified = value;
    }

    /**
     * Ruft den Wert der isExported-Eigenschaft ab.
     * 
     */
    public boolean isIsExported() {
        return isExported;
    }

    /**
     * Legt den Wert der isExported-Eigenschaft fest.
     * 
     */
    public void setIsExported(boolean value) {
        this.isExported = value;
    }

    /**
     * Ruft den Wert der isTransmitted-Eigenschaft ab.
     * 
     */
    public boolean isIsTransmitted() {
        return isTransmitted;
    }

    /**
     * Legt den Wert der isTransmitted-Eigenschaft fest.
     * 
     */
    public void setIsTransmitted(boolean value) {
        this.isTransmitted = value;
    }

    /**
     * Ruft den Wert der transmissionDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransmissionDate() {
        return transmissionDate;
    }

    /**
     * Legt den Wert der transmissionDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransmissionDate(XMLGregorianCalendar value) {
        this.transmissionDate = value;
    }

    /**
     * Ruft den Wert der approvalStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * Legt den Wert der approvalStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalStatus(String value) {
        this.approvalStatus = value;
    }

    /**
     * Ruft den Wert der note-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Legt den Wert der note-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

}
