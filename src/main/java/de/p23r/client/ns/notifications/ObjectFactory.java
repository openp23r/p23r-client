//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.02.13 um 11:15:14 AM CET 
//


package de.p23r.client.ns.notifications;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.p23r.client.ns.notifications package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IsExported_QNAME = new QName("http://client.p23r.de/ns/notifications", "isExported");
    private final static QName _IsModified_QNAME = new QName("http://client.p23r.de/ns/notifications", "isModified");
    private final static QName _NotificationId_QNAME = new QName("http://client.p23r.de/ns/notifications", "notificationId");
    private final static QName _NotificationRuleId_QNAME = new QName("http://client.p23r.de/ns/notifications", "notificationRuleId");
    private final static QName _IsTransmitted_QNAME = new QName("http://client.p23r.de/ns/notifications", "isTransmitted");
    private final static QName _Note_QNAME = new QName("http://client.p23r.de/ns/notifications", "note");
    private final static QName _ApprovalStatus_QNAME = new QName("http://client.p23r.de/ns/notifications", "approvalStatus");
    private final static QName _EntryDate_QNAME = new QName("http://client.p23r.de/ns/notifications", "entryDate");
    private final static QName _DocumentName_QNAME = new QName("http://client.p23r.de/ns/notifications", "documentName");
    private final static QName _IsNew_QNAME = new QName("http://client.p23r.de/ns/notifications", "isNew");
    private final static QName _OriginalDocumentName_QNAME = new QName("http://client.p23r.de/ns/notifications", "originalDocumentName");
    private final static QName _TransmissionDate_QNAME = new QName("http://client.p23r.de/ns/notifications", "transmissionDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.p23r.client.ns.notifications
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

    /**
     * Create an instance of {@link Notifications }
     * 
     */
    public Notifications createNotifications() {
        return new Notifications();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "isExported", defaultValue = "false")
    public JAXBElement<Boolean> createIsExported(Boolean value) {
        return new JAXBElement<Boolean>(_IsExported_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "isModified", defaultValue = "false")
    public JAXBElement<Boolean> createIsModified(Boolean value) {
        return new JAXBElement<Boolean>(_IsModified_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "notificationId")
    public JAXBElement<String> createNotificationId(String value) {
        return new JAXBElement<String>(_NotificationId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "notificationRuleId")
    public JAXBElement<String> createNotificationRuleId(String value) {
        return new JAXBElement<String>(_NotificationRuleId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "isTransmitted", defaultValue = "false")
    public JAXBElement<Boolean> createIsTransmitted(Boolean value) {
        return new JAXBElement<Boolean>(_IsTransmitted_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "note")
    public JAXBElement<String> createNote(String value) {
        return new JAXBElement<String>(_Note_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "approvalStatus")
    public JAXBElement<String> createApprovalStatus(String value) {
        return new JAXBElement<String>(_ApprovalStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "entryDate")
    public JAXBElement<XMLGregorianCalendar> createEntryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_EntryDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "documentName")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createDocumentName(String value) {
        return new JAXBElement<String>(_DocumentName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "isNew", defaultValue = "true")
    public JAXBElement<Boolean> createIsNew(Boolean value) {
        return new JAXBElement<Boolean>(_IsNew_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "originalDocumentName")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createOriginalDocumentName(String value) {
        return new JAXBElement<String>(_OriginalDocumentName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/notifications", name = "transmissionDate")
    public JAXBElement<XMLGregorianCalendar> createTransmissionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransmissionDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

}
