//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.02.13 um 11:15:14 AM CET 
//


package de.p23r.client.ns.ruleinfos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.p23r.client.ns.ruleinfos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AutoNotification_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "autoNotification");
    private final static QName _RulePackageID_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "rulePackageID");
    private final static QName _RuleID_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "ruleID");
    private final static QName _AutoNotificationPassword_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "autoNotificationPassword");
    private final static QName _AutoNotificationUser_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "autoNotificationUser");
    private final static QName _Manifest_QNAME = new QName("http://client.p23r.de/ns/ruleinfos", "manifest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.p23r.client.ns.ruleinfos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RuleInfo }
     * 
     */
    public RuleInfo createRuleInfo() {
        return new RuleInfo();
    }

    /**
     * Create an instance of {@link RuleInfos }
     * 
     */
    public RuleInfos createRuleInfos() {
        return new RuleInfos();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "autoNotification")
    public JAXBElement<Boolean> createAutoNotification(Boolean value) {
        return new JAXBElement<Boolean>(_AutoNotification_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "rulePackageID")
    public JAXBElement<String> createRulePackageID(String value) {
        return new JAXBElement<String>(_RulePackageID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "ruleID")
    public JAXBElement<String> createRuleID(String value) {
        return new JAXBElement<String>(_RuleID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "autoNotificationPassword")
    public JAXBElement<String> createAutoNotificationPassword(String value) {
        return new JAXBElement<String>(_AutoNotificationPassword_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "autoNotificationUser")
    public JAXBElement<String> createAutoNotificationUser(String value) {
        return new JAXBElement<String>(_AutoNotificationUser_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://client.p23r.de/ns/ruleinfos", name = "manifest")
    public JAXBElement<String> createManifest(String value) {
        return new JAXBElement<String>(_Manifest_QNAME, String.class, null, value);
    }

}
