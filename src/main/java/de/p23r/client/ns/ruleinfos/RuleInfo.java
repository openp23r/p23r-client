//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.02.13 um 11:15:14 AM CET 
//


package de.p23r.client.ns.ruleinfos;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}rulePackageID"/>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}ruleID"/>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}manifest"/>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}autoNotification"/>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}autoNotificationUser"/>
 *         &lt;element ref="{http://client.p23r.de/ns/ruleinfos}autoNotificationPassword"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rulePackageID",
    "ruleID",
    "manifest",
    "autoNotification",
    "autoNotificationUser",
    "autoNotificationPassword"
})
@XmlRootElement(name = "ruleInfo")
public class RuleInfo
    implements Serializable
{

    private final static long serialVersionUID = 12343L;
    @XmlElement(required = true)
    protected String rulePackageID;
    @XmlElement(required = true)
    protected String ruleID;
    @XmlElement(required = true)
    protected String manifest;
    protected boolean autoNotification;
    @XmlElement(required = true)
    protected String autoNotificationUser;
    @XmlElement(required = true)
    protected String autoNotificationPassword;

    /**
     * Ruft den Wert der rulePackageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRulePackageID() {
        return rulePackageID;
    }

    /**
     * Legt den Wert der rulePackageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRulePackageID(String value) {
        this.rulePackageID = value;
    }

    /**
     * Ruft den Wert der ruleID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleID() {
        return ruleID;
    }

    /**
     * Legt den Wert der ruleID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleID(String value) {
        this.ruleID = value;
    }

    /**
     * Ruft den Wert der manifest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManifest() {
        return manifest;
    }

    /**
     * Legt den Wert der manifest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManifest(String value) {
        this.manifest = value;
    }

    /**
     * Ruft den Wert der autoNotification-Eigenschaft ab.
     * 
     */
    public boolean isAutoNotification() {
        return autoNotification;
    }

    /**
     * Legt den Wert der autoNotification-Eigenschaft fest.
     * 
     */
    public void setAutoNotification(boolean value) {
        this.autoNotification = value;
    }

    /**
     * Ruft den Wert der autoNotificationUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoNotificationUser() {
        return autoNotificationUser;
    }

    /**
     * Legt den Wert der autoNotificationUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoNotificationUser(String value) {
        this.autoNotificationUser = value;
    }

    /**
     * Ruft den Wert der autoNotificationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoNotificationPassword() {
        return autoNotificationPassword;
    }

    /**
     * Legt den Wert der autoNotificationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoNotificationPassword(String value) {
        this.autoNotificationPassword = value;
    }

}
